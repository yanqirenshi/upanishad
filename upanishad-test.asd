(in-package :cl-user)
(defpackage upanishad-test-asd
  (:use :cl :asdf))
(in-package :upanishad-test-asd)

(defsystem :upanishad-test
  :name "UPANISHAD-TEST"
  :author "yanqirenshi"
  :version "0.1"
  :maintainer "yanqirenshi"
  :licence "Lesser Lisp General Public License"
  :description "Common Lisp Prevalence Test Package"
  :long-description "5am test suite for cl-prevalence"
  :components
  ((:module "t"
    :components ((:file "utility")
                 (:file "package")
                 (:file "upanishad")
                 (:module "pool"
                  :components ((:file "package")
                               (:module "core" :components ((:file "package")
                                                            (:file "core")))
                               (:module "librarian" :components ((:file "package")
                                                                 (:file "librarian")))
                               (:module "secretary" :components ((:file "package")
                                                                 (:file "secretary")
                                                                 (:file "transaction-log")))))
                 (:module "persistence"
                  :components ((:file "package")
                               (:file "xml"))))))
  :depends-on (:upanishad :prove)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove.asdf) c)
                    (asdf:clear-system c)))
