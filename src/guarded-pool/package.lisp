(in-package :cl-user)
(defpackage :upanishad.guarded-pool
  (:nicknames :up.guarded-pool)
  (:use #:cl)
  (:import-from :upanishad.pool
                #:pool)
  (:import-from :upanishad.pool.secretary
                #:transaction)
  (:documentation ""))
(in-package :upanishad.guarded-pool)
