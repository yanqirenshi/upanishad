(in-package :cl-user)

(defpackage :upanishad.memes
  (:nicknames :up.memes)
  (:use :cl)
  (:import-from :upanishad.meme
                #:meme
                #:%id)
  (:import-from :upanishad.index
                #:make-slot-index
                #:get-at-value
                #:add-object
                #:remove-object)
  (:import-from :upanishad.persistence
                #:write-xml-tag-slot
                #:print-symbol-xml
                #:serialize-xml-internal)
  (:export #:memes
           #:meme-class
           #:meme-list
           #:%id-index
           #:get-meme
           #:find-meme
           #:add-meme
           #:make-memes
           #:remove-meme)
  (:documentation ""))

(in-package :upanishad.memes)
