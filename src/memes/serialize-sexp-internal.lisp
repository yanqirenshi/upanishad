(in-package :upanishad.memes)

;;;;;
;;;;; INDEX TAG
;;;;;
(defun write-xml-tag-memes-befor (stream memes)
  (let ((class (class-name (class-of memes))))
    (write-string "<MEMES CLASS=\"" stream)
    (print-symbol-xml class stream)
    (princ "\">" stream)))

(defun write-xml-tag-memes-after (stream)
  (write-string "</MEMES>" stream))


;;;;;
;;;;; SERIALIZE-XML-INTERNAL
;;;;;
(defmethod serialize-xml-internal ((memes memes) stream serialization-state)
  (write-xml-tag-memes-befor stream memes)
  (write-xml-tag-slot stream memes 'meme-class serialization-state)
  (write-xml-tag-slot stream memes 'meme-list  serialization-state)
  (write-xml-tag-memes-after stream))
