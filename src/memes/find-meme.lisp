(in-package :upanishad.memes)

(defun %find-meme (slot value meme-list)
  (alexandria:when-let ((meme (car meme-list)))
    (if (not (equalp (slot-value meme slot) value))
        (%find-meme slot value (cdr meme-list))
        (cons meme (%find-meme slot value (cdr meme-list))))))

(defgeneric find-meme (memes &key slot value)
  (:documentation "list から slot = value の meme のリストを返します。")
  (:method ((memes memes) &key slot value)
    (if (null slot)
        (meme-list memes)
        (%find-meme slot value (meme-list memes)))))
