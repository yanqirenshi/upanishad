(in-package :cl-user)

(defpackage :upanishad.index
  (:nicknames :up.index)
  (:use :cl)
  (:import-from :upanishad.meme
                #:%id)
  (:import-from :upanishad.persistence
                #:write-xml-tag-slot
                #:print-symbol-xml
                #:serialize-xml-internal)
  (:export #:index)
  (:export #:slot-index
           #:class-symbol
           #:slot-symbol
           #:slot-index-type-p)
  (:export #:slot-index-unique
           #:value->object)
  (:export #:slot-index-multiple
           #:value->objects)
  (:export #:make-slot-index
           #:get-index-key
           #:get-at-value
           #:add-object
           #:add-objects
           #:remove-object
           #:change-object)
  (:documentation ""))
