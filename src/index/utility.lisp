(in-package :upanishad.index)

(defun assert-class (class meme)
  (unless (eq class (type-of meme))
    (error "index is not meme's slot index")))
