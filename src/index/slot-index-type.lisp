(in-package :upanishad.index)

(defparameter *slot-index-types*
  '((:unique slot-index-unique)
    (:multiple slot-index-multiple)))

(defun slot-index-type-p (type)
  (not (null (assoc type *slot-index-types*))))

(defun get-slot-index-class (type)
  (cadr (assoc type *slot-index-types*)))
