(in-package :upanishad.index)

;;;;;
;;;;; INDEX TAG
;;;;;
(defun write-xml-tag-index-befor (stream index)
  (let ((class (class-name (class-of index))))
    (write-string "<INDEX CLASS=\"" stream)
    (print-symbol-xml class stream)
    (princ "\">" stream)))

(defun write-xml-tag-index-after (stream)
  (write-string "</INDEX>" stream))


;;;;;
;;;;; SERIALIZE-XML-INTERNAL
;;;;;
(defmethod serialize-xml-internal ((index slot-index) stream serialization-state)
  (write-xml-tag-index-befor stream index)
  (write-xml-tag-slot stream index 'class-symbol serialization-state)
  (write-xml-tag-slot stream index 'slot-symbol  serialization-state)
  (write-xml-tag-index-after stream))
