(in-package :cl-user)
(defpackage :upanishad
  (:nicknames :up)
  (:use #:cl)
  ;; :upanishad.meme
  (:import-from :upanishad.meme
                #:%id
                #:meme)
  ;; :upanishad.pool
  (:import-from :upanishad.pool
                #:pool
                #:make-pool
                #:start
                #:stop
                #:snapshot
                #:restore
                #:totally-destroy)
  (:import-from :upanishad.pool
                #:tx-create-meme
                #:tx-update-meme
                #:tx-delete-meme
                #:find-meme
                #:get-meme)
  (:import-from :upanishad.pool
                #:get-meme-core
                #:get-memes-core
                #:find-meme-core
                #:tx-add-memes-core
                #:tx-create-meme-core
                #:tx-update-meme-core
                #:tx-delete-meme-core)
  (:import-from :upanishad.pool
                #:get-index
                #:tx-add-index
                #:tx-remove-index)
  (:import-from :upanishad.pool
                #:execute-transaction)
  ;; from :upanishad.meme
  (:export #:%id
           #:meme)
  ;; from :upanishad.pool
  (:export #:pool
           #:make-pool
           #:start
           #:stop
           #:totally-destroy)
  (:export #:tx-create-meme
           #:tx-update-meme
           #:tx-delete-meme
           #:find-meme
           #:get-meme)
  (:export #:get-index
           #:tx-add-index
           #:tx-remove-index)
  (:export #:execute-transaction)
  (:documentation ""))
(in-package :upanishad)
