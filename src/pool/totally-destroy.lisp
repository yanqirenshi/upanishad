(in-package :upanishad.pool)

(defgeneric delete-all-files (pool)
  (:method ((pool pool))
    (clear-snapshot-backups pool)
    (clear-snapshots pool)
    (clear-transaction-log-file pool)
    (clear-transaction-log-file-backup pool)))

(defgeneric clear-all-objects (pool)
  (:method ((pool pool))
    (clear-%id pool)
    (clear-memes pool)
    (clear-indexes pool)
    (clear-options pool)
    (clear-preferences pool)))

(defgeneric totally-destroy (pool &key abort)
  (:documentation "Totally destroy pool from permanent storage by deleting any files that we find")
  (:method ((pool pool) &key abort)
    "Totally destroy pool from permanent storage by deleting any files used by the pool, remove all root objects"
    (stop pool :abort abort)
    (delete-all-files pool)
    (clear-all-objects pool)))
