(in-package :upanishad.pool)

(defun make-snapshot-types (additional-snapshot-types)
  (let ((befor '((:id         . up.pool.id::%id)
                 (:option     . up.pool.option::options)
                 (:preference . up.pool.preference::preferences)
                 (:memes      . up.pool.core::memes)))
        (after '((:indexes    . up.pool.core::indexes))))
    (nconc (copy-list befor)
           (copy-list additional-snapshot-types)
           (copy-list after))))

(defmethod initialize-instance :after ((pool pool) &rest initargs &key &allow-other-keys)
  (declare (ignore initargs))
  (init-secretary pool)
  (init-librarian pool (snapshot-types pool))
  (restore pool))

(defun make-pool (directory &key (pool-class 'pool) snapshot-types init-args)
  (apply #'make-instance pool-class
         :snapshot-directory directory
         :snapshot-directory-backup directory
         :transaction-log-directory directory
         :transaction-log-directory-backup directory
         :snapshot-types (make-snapshot-types snapshot-types)
         init-args))
