(in-package :upanishad.pool.preference)

(defgeneric clear-preferences (pool)
  (:method ((pool pool-preference))
    (setf (preferences pool) (make-hash-table))))

(defgeneric get-preference (pool key)
  (:documentation "Retrieve the value of the persistent preference stored under key in pool")
  (:method ((pool pool-preference) key)
    (let ((preferences (preferences pool)))
      (when preferences
        (gethash key preferences)))))

(defgeneric tx-set-preference (pool key value)
  (:method ((pool pool-preference) key value)
    (let ((preferences (preferences pool)))
      (when (not preferences)
        (setf preferences        (make-hash-table)
              (preferences pool) preferences))
      (setf (gethash key preferences) value))))

(defgeneric all-preferences-keys (pool)
  (:documentation "Return a list of all persistent preference keys of pool")
  (:method ((pool pool-preference))
    (let ((preferences (preferences pool)))
      (when preferences
        (let (keys)
          (maphash #'(lambda (key value)
                       (declare (ignore value))
                       (push key keys))
                   preferences)
          keys)))))
