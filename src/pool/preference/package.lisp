(in-package :cl-user)
(defpackage :upanishad.pool.preference
  (:nicknames :up.pool.preference)
  (:use #:cl)
  (:export #:pool-preference
           #:clear-preferences)
  (:documentation ""))
(in-package :upanishad.pool.preference)
