(in-package :upanishad.pool.preference)

(defclass pool-preference ()
  ((preferences
    :documentation ""
    :accessor preferences
    :initform (make-hash-table)
    :type 'hash-table)))
