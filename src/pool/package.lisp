(in-package :cl-user)
(defpackage :upanishad.pool
  (:use #:cl
        #:upanishad.persistence
        #:upanishad.meme
        #:upanishad.pool.secretary
        #:upanishad.pool.librarian
        #:upanishad.pool.core
        #:upanishad.pool.id
        #:upanishad.pool.option
        #:upanishad.pool.preference)
  (:nicknames :up.pool)
  (:import-from #:alexandria
                #:ensure-gethash
                #:when-let)
  (:import-from #:upanishad.memes
                #:meme-class)
  (:import-from :upanishad.utilities
                #:truncate-file
                #:timetag)
  (:export #:pool
           #:make-pool
           #:start
           #:stop)
  (:export #:snapshot
           #:restore
           #:totally-destroy)
  (:export #:tx-create-meme
           #:tx-update-meme
           #:tx-delete-meme
           #:find-meme
           #:get-meme)
  (:export #:get-meme-core
           #:get-memes-core
           #:find-meme-core
           #:tx-add-memes-core
           #:tx-create-meme-core
           #:tx-update-meme-core
           #:tx-delete-meme-core)
  (:export #:get-index
           #:tx-add-index
           #:tx-remove-index)
  (:export #:execute-transaction)
  (:documentation "An implementation of Object Prevalence for Common Lisp"))
