(in-package :upanishad.pool)

(defgeneric start (pool)
  (:documentation "Stop a pool")
  (:method ((pool pool))
    (open-transaction-log-stream pool)
    pool))
