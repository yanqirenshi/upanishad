(in-package :cl-user)
(defpackage :upanishad.pool.core
  (:use #:cl)
  (:nicknames :up.pool.core)
  (:import-from :upanishad.index
                #:make-slot-index)
  (:export #:pool-core)
  (:export #:memes
           #:indexes)
  (:export #:get-index
           #:tx-add-index
           #:tx-remove-index
           #:ensure-index
           #:clear-indexes)
  (:export #:get-memes
           #:tx-add-memes
           #:tx-remove-memes
           #:ensure-memes
           #:clear-memes)
  (:export #:get-memes-core
           #:tx-add-memes-core
           #:ensure-memes-core)
  (:documentation ""))
(in-package :upanishad.pool.core)
