(in-package :upanishad.pool.core)

(defgeneric clear-memes (pool)
  (:method ((pool pool-core))
    (setf (memes pool) (make-hash-table :test 'eq))))

(defun %get-memes (pool class)
  (let ((ht (memes pool))
        (key class))
    (when ht
      (gethash key ht))))

(defun get-memes-core (pool &key class memes-slot)
  (let ((ht (slot-value pool memes-slot))
        (key class))
    (when ht
      (gethash key ht))))

(defgeneric get-memes (pool &key class)
  (:documentation "プールの memes スロットから memes クラスのインスタンスを取得する。")
  (:method ((pool pool-core) &key class)
    (get-memes-core pool
                    :class class
                    :memes-slot 'memes)))

(defun tx-add-memes-core (pool memes &key memes-slot)
  (let ((memes-ht (slot-value pool memes-slot))
        (key (up.memes:meme-class memes)))
    (assert (null (gethash key memes-ht)))
    (setf (gethash key memes-ht) memes)
    memes))

(defgeneric tx-add-memes (pool memes)
  (:documentation "プールの memes スロットに memes を追加する。")
  (:method ((pool pool-core) (class symbol))
    (let ((memes (up.memes:make-memes class)))
      (tx-add-memes pool memes)))
  (:method ((pool pool-core) (memes up.memes:memes))
    (tx-add-memes-core pool memes :memes-slot 'memes)))

(defgeneric tx-remove-memes (pool memes)
  (:method ((pool pool-core) (memes up.memes:memes))
    (tx-remove-memes pool (up.memes:meme-class memes)))
  (:method ((pool pool-core) (class symbol))
    (let ((ht (memes pool))
          (key class))
      (when (gethash key ht)
        (remhash key ht))))
  (:documentation "プールの memes スロットから memes を削除する。"))


(defun ensure-memes-core (pool class &key memes-slot)
  (or (get-memes-core pool :class class :memes-slot memes-slot)
      (let ((memes (up.memes:make-memes class)))
        (tx-add-memes-core pool memes :memes-slot memes-slot))))

(defgeneric ensure-memes (pool &key class)
  (:method ((pool pool-core) &key class)
    (ensure-memes-core pool class :memes-slot 'memes)))
