(in-package :upanishad.pool.core)

(defgeneric clear-indexes (pool)
  (:method ((pool pool-core))
    (setf (indexes pool) (make-hash-table :test 'eq))))

(defgeneric get-index (pool class slot)
  (:method ((pool pool-core) (class symbol) (slot symbol))
    (let* ((class-ht (indexes pool))
           (slot-ht (gethash class class-ht)))
      (when slot-ht
        (gethash slot slot-ht))))
  (:documentation "プールに登録されているスロット・インデックスを取得します。"))

;;;;;
;;;;; tx-add-index
;;;;;
(defun %tx-add-index (indexes class slot index)
  (let* ((class-ht indexes)
         (slot-ht (alexandria:ensure-gethash class class-ht
                                             (make-hash-table))))
    (when (gethash slot slot-ht)
      (error "aledy exist index"))
    (setf (gethash slot slot-ht) index)))

(defgeneric tx-add-index (pool index-or-params)
  (:method ((pool pool-core) (params list))
    (let ((class (getf params :class))
          (slot (getf params :slot))
          (type (getf params :type)))
      (tx-add-index pool (make-slot-index class slot type))))
  (:method ((pool pool-core) (index up.index:index))
    (multiple-value-bind (class slot)
        (up.index:get-index-key index)
      (let ((indexes (indexes pool)))
        (when (get-index pool class slot)
          (error "Aledy exist index"))
        (%tx-add-index indexes class slot index))))
  (:documentation "プールにスロット・インデックスを登録します。"))

;;;;;
;;;;; tx-remove-index
;;;;;
(defun %tx-remove-index (pool class slot)
  (assert (or pool class slot))
  (let* ((classht (indexes pool))
         (slotht (gethash class classht)))
    (when (gethash slot slotht)
      (remhash slot slotht)
      (when (= (hash-table-count slotht) 0)
        (remhash class classht)))))

(defgeneric tx-remove-index (pool index)
  (:method ((pool pool-core) (index up.index:index))
    (multiple-value-bind (class slot)
        (up.index:get-index-key index)
      (%tx-remove-index pool class slot))
    pool)
  (:documentation "プールからスロット・インデックスを削除します。"))


;;;;;
;;;;; ensure
;;;;;
(defun ensure-index (pool class slot type)
  (or (get-index pool class slot)
      (tx-add-index pool (list :class class :slot slot :type type))))
