(in-package :upanishad.pool.core)

(defclass pool-core ()
  ((memes
    :documentation ":type hash-table"
    :accessor memes
    :initform (make-hash-table :test 'eq)
    :type 'hash-table)
   (indexes
    :documentation ":type hash-table"
    :accessor indexes
    :initform (make-hash-table :test 'eq)
    :type 'hash-table)))
