(in-package :upanishad.pool.option)

(defgeneric clear-options (pool)
  (:method ((pool pool-option))
    (setf (options pool) (make-init-options))))

(defmethod get-option ((pool pool-option) name)
  (with-slots (options) pool
    (gethash name options)))

(defmethod (setf get-option) (value (pool pool-option) name)
  (with-slots (options) pool
    (setf (gethash name options) value)))
