(in-package :upanishad.pool.option)

(defvar *default-options*
  '((:code :rollback-on-error :default-value nil :documentation "execute でつかっている。")))

(defun make-init-options ()
  (let ((ht (make-hash-table)))
    (dolist (option *default-options*)
      (let ((code (getf option :code))
            (value (getf option :default-value)))
        (setf (gethash code ht) value)))
    ht))

(defclass pool-option ()
  ((options
    :documentation ""
    :accessor options
    :initform (make-init-options)
    :type 'hash-table)))
