(in-package :upanishad.pool)

(defgeneric tx-add-meme-to-index (pool slot meme)
  (:documentation "インデックスにミームヲ追加します。")
  (:method ((pool pool) (slot symbol) (meme meme))
    (let ((index (get-index pool (class-name (class-of meme)) slot)))
      (when index
        (up.index:add-object index meme)))))

(defgeneric tx-remove-meme-from-index (pool slot meme)
  (:documentation "インデックスからミームを削除します。")
  (:method ((pool pool) (slot symbol) (meme meme))
    (let ((index (get-index pool (type-of meme) slot)))
      (when index
        (up.index:remove-object index meme)))))
