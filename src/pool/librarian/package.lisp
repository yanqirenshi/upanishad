(in-package :cl-user)

(defpackage :upanishad.pool.librarian
  (:nicknames :up.pool.librarian)
  (:documentation "")
  (:use #:cl
        #:upanishad.persistence)
  (:import-from :alexandria
                #:when-let)
  (:import-from :fad
                #:file-exists-p)
  (:import-from :upanishad.utilities
                #:copy-file
                #:timetag
                #:rm-directory-files)
  (:export #:librarian
           #:init-librarian)
  (:export #:restore-snapshots
           #:backup-snapshots
           #:snapshot-objects)
  (:export #:clear-snapshots
           #:clear-snapshot-backups))
(in-package :upanishad.pool.librarian)
