(in-package :upanishad.pool.librarian)

(defun set-pathname (pool type directory)
  "pool の snapshot に pathname を追加します。"
  (setf (snapshot-pathnames pool type)
        (make-snapshot-pathname pool directory type)))

(defun init-snapshot-pathname (pool directory snapshot-types)
  "pool の snapshot を初期化します。"
  (dolist (type (mapcar #'car snapshot-types))
    (set-pathname pool type directory)))

(defun init-librarian (pool snapshot-types)
  (with-slots (snapshot-directory)
      pool
    (assert snapshot-directory)
    (ensure-directories-exist snapshot-directory)
    (init-snapshot-pathname pool snapshot-directory snapshot-types)))
