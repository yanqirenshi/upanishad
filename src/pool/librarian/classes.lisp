(in-package :upanishad.pool.librarian)

(defclass librarian ()
  ((snapshot-file-extension
    :accessor snapshot-file-extension
    :initarg :snapshot-file-extension
    :initform "xml"
    :type 'string)
   (snapshot-directory
    :accessor snapshot-directory
    :initarg :snapshot-directory
    :initform nil
    :type 'pathname)
   (snapshot-directory-backup
    :accessor snapshot-directory-backup
    :initarg :snapshot-directory-backup
    :initform nil
    :type 'pathname)
   (snapshot
    :documentation ""
    :initform (make-hash-table :test 'eq)
    :type 'pathname)
   (snapshot-serializer
    :documentation ""
    :accessor snapshot-serializer
    :initarg :snapshot-serializer
    :type 'function
    :initform #'serialize-xml)
   (snapshot-serialization-state
    :documentation ""
    :reader snapshot-serialization-state
    :initform (make-serialization-state))
   (snapshot-deserializer
    :documentation ""
    :accessor snapshot-deserializer
    :initarg :snapshot-deserializer
    :initform #'deserialize-xml)))
