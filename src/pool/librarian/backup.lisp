(in-package :upanishad.pool.librarian)

(defun backup-snapshot (librarian type-code)
  (let* ((timetag (timetag))
         (snapshot-file (snapshot-pathnames librarian type-code)))
    (snapshot-copy-snapshot-file librarian snapshot-file type-code timetag)))

(defun %backup-snapshots (librarian snapshot-types)
  (let ((snapshot-type (car snapshot-types)))
    (if (not snapshot-type)
        librarian
        (let ((type-code (car snapshot-type)))
          (backup-snapshot librarian type-code)
          (%backup-snapshots librarian (cdr snapshot-types))))))

(defgeneric backup-snapshots (librarian snapshot-types)
  (:method ((librarian librarian) (snapshot-types list))
    (%backup-snapshots librarian snapshot-types)))

(defgeneric clear-snapshot-backups (librarian)
  (:method ((librarian librarian))
    (rm-directory-files (snapshot-directory-backup librarian)
                        (snapshot-file-extension librarian))
    librarian))
