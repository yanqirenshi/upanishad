(in-package :upanishad.pool.librarian)

(defun desirialize-snapshot (pool snapshot-file)
  (let ((deserializer (snapshot-deserializer pool))
        (serialization-state (snapshot-serialization-state pool)))
    (when (file-exists-p snapshot-file)
      (with-open-file (in snapshot-file
                          :direction :input)
        (funcall deserializer in serialization-state)))))
