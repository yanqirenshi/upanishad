(in-package :upanishad.pool.librarian)

(defmethod snapshot-pathnames (pool type)
  (let ((pathnames (slot-value pool 'snapshot)))
    (gethash type pathnames)))

(defmethod (setf snapshot-pathnames) (value pool type)
  (let ((pathnames (slot-value pool 'snapshot)))
    (setf (gethash type pathnames) value)))

(defgeneric make-snapshot-filename (pool type &optional suffix)
  (:documentation "snapshot のファイル名を返す。")
  (:method (pool type &optional suffix)
    (unless type (warn "type が nil ですよ。"))
    (if type
        (format nil "snapshot-~a~@[-~a~]" (string-downcase (symbol-name type)) suffix)
        (format nil "snapshot~@[-~a~]" suffix))))

(defgeneric make-snapshot-pathname (pool directory type &optional suffix)
  (:documentation "snapshot のファイル名をフルパスで返す。")
  (:method (pool directory type &optional suffix)
    (let ((filename (make-snapshot-filename pool type suffix))
          (file-extension (snapshot-file-extension pool)))
      (merge-pathnames (make-pathname :name filename
                                      :type file-extension)
                       directory))))

(defun make-snapshot-backup-pathname (pool directory type timetag)
  "snapshot のバックアップ・ファイル名を返します。"
  (make-snapshot-pathname pool directory type timetag))

(defun snapshot-copy-snapshot-file (pool directory type timetag)
  ""
  (when (probe-file directory)
    (copy-file directory
               (make-snapshot-backup-pathname pool directory type timetag))))
