(in-package :upanishad.pool.librarian)

(defun restore-snapshot (librarian type-code slot-symbol)
  (let* ((snapshot-pathnames (snapshot-pathnames librarian type-code))
         (objects (desirialize-snapshot librarian snapshot-pathnames)))
    (when objects
      (setf (slot-value librarian slot-symbol) objects))))

(defun %restore-snapshots (librarian snapshot-types)
  (when-let ((snapshot-type (car snapshot-types)))
    (let ((type-code (car snapshot-type))
          (slot-symbol (cdr snapshot-type)))
      (restore-snapshot librarian type-code slot-symbol)
      (%restore-snapshots librarian (cdr snapshot-types)))))

(defgeneric restore-snapshots (librarian snapshot-types)
  (:method ((librarian librarian) (snapshot-types list))
    (%restore-snapshots librarian snapshot-types)
    librarian))
