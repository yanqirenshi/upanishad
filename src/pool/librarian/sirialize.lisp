(in-package :upanishad.pool.librarian)

(defun sirialize-objects (pool state snapshot objects)
  "type で指定された Objects をシリアライズする。
シリアライズしたものを snapshot に書き込む。"
  (let ((serializer (snapshot-serializer pool)))
    (with-open-file (out snapshot :direction         :output
                                  :if-does-not-exist :create
                                  :if-exists         :supersede)
      (funcall serializer objects out state))))
