(in-package :upanishad.pool)

(defgeneric stop (pool &key abort)
  (:documentation "Stop a pool")
  (:method ((pool pool) &key abort)
    (close-transaction-log-stream pool :abort abort)
    pool))
