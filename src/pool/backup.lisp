(in-package :upanishad.pool)

(defgeneric backup (pool)
  (:documentation "Make backup copies of the current snapshot and transaction-log files")
  (:method ((pool pool))
    (close-transaction-log-stream pool)
    (backup-snapshots pool (snapshot-types pool))
    (backup-transaction pool)
    pool))
