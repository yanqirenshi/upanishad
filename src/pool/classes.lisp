(in-package :upanishad.pool)

(defclass pool (pool-id
                pool-core
                pool-option
                pool-preference
                secretary
                librarian)
  ((snapshot-types
    :documentation ""
    :accessor snapshot-types
    :initarg :snapshot-types
    :initform nil
    :type 'list))
  (:documentation "Base Prevalence system implementation object"))
