(in-package :upanishad.pool)

(defgeneric query (pool function &rest args)
  (:documentation "Ask for a query function to be executed on pool with args")
  (:method ((pool pool) function &rest args)
    "Execute an exclusive query function on a sytem"
    (apply function (cons pool args))))
