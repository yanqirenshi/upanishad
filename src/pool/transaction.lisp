(in-package :upanishad.pool)

(defgeneric execute (pool object)
  (:documentation "Ask for a transaction object to be executed on pool with ACID properties")
  (:method ((pool pool) (transaction transaction))
    "Execute a transaction on a pool and log it to the transaction log"
    (let ((result (multiple-value-list
                   (handler-bind ((error #'(lambda (condition)
                                             (when (and (get-option pool :rollback-on-error)
                                                        (initiates-rollback condition))
                                               (format *standard-output*
                                                       ";; Notice: pool rollback/restore due to error (~a)~%"
                                                       condition)
                                               (restore pool)))))
                     (execute-on transaction pool)))))
      (log-transaction pool transaction)
      (apply #'values result))))


(defmacro execute-transaction (transaction-call)
  `(if (not (poolp ,(second transaction-call)))
       (error "第一引数が pool ではありません。第一引数=~a" ,(second transaction-call))
       (execute ,(second transaction-call)
                (make-transaction ',(first transaction-call) ,@(rest (rest transaction-call))))))
