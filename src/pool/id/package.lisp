(in-package :cl-user)
(defpackage :upanishad.pool.id
  (:nicknames :up.pool.id)
  (:use #:cl)
  (:export #:pool-id
           #:next-%id
           #:clear-%id)
  (:documentation ""))
(in-package :upanishad.pool.id)
