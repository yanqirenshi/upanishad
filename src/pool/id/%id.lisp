(in-package :upanishad.pool.id)

(defgeneric clear-%id (pool)
  (:method ((pool pool-id))
    (setf (%id pool) 0)))

(defmethod next-%id ((pool pool-id))
  (incf (%id pool)))
