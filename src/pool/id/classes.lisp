(in-package :upanishad.pool.id)

(defclass pool-id ()
  ((%id
    :documentation ""
    :accessor %id
    :initform 0
    :type 'integer)))
