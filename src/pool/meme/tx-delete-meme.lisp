(in-package :upanishad.pool)

(defun tx-delete-meme-core (pool class %id &key memes-slot)
  "拡張用。 export していないけど。"
  (let ((memes (up.pool.core::get-memes-core pool :class class :memes-slot memes-slot))
        (meme (get-meme-core pool class :%id %id :memes-slot memes-slot)))
    (if meme
        (progn
          ;; TODO: remove all index
          (up.memes:remove-meme memes meme))
        (error "no meme of class ~a with %id ~d found in ~s" class %id pool))
    pool))

(defgeneric tx-delete-meme (pool class %id)
  (:documentation "プールから meme を削除する。")
  (:method ((pool pool) (class symbol) (%id integer))
    (tx-delete-meme-core pool class %id :memes-slot 'memes)))
