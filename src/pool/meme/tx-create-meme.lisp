(in-package :upanishad.pool)

(defun make-meme (pool class params)
  (let* ((%id (next-%id pool))
         (meme (make-instance class :%id %id)))
    (update-meme-slots-by-alist meme params)
    meme))

(defun find-all-slots (params)
  ;; TODO: これは meme から slot を取得してやるべきだけど。。
  (mapcar #'car params))

(defun tx-create-meme-core (pool memes class &key params)
  (let ((meme (make-meme pool class params))
        (slots (find-all-slots params)))
    (update-meme-slots-index pool meme slots)
    (up.memes:add-meme memes meme)
    meme))

(defgeneric tx-create-meme (pool class &optional params)
  (:documentation "meme を新規に作成しプールに追加する。")
  (:method ((pool pool) class &optional params)
    (let ((memes (ensure-memes pool :class class)))
      (tx-create-meme-core pool memes class :params params))))
