(in-package :upanishad.pool)

(defun update-meme-slots-index (pool meme slots)
  (when-let ((slot (car slots)))
    (tx-remove-meme-from-index pool slot meme)
    (tx-add-meme-to-index pool slot meme)
    (update-meme-slots-index pool meme (cdr slots)))
  meme)
