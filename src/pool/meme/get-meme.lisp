(in-package :upanishad.pool)

(defun get-meme-at-slot (pool class slot value)
  (let ((index (get-index pool class slot)))
    (if index
        (up.index:get-at-value index value)
        (up.memes:get-meme (get-memes pool :class class)
                           :slot slot
                           :value value))))

(defun get-meme-core (pool class &key %id memes-slot)
  "拡張用。 export していないけど。"
  (let ((memes (up.pool.core::get-memes-core pool :class class :memes-slot memes-slot)))
    (cond (%id (up.memes:get-meme memes :%id %id))
          (t nil))))

(defgeneric get-meme (pool class &key %id)
  (:documentation "プールからmemeを取得する。
しかし、これは find-meme との差がつかない。
get - find の名前に問題(限界)があるのかな。。。")
  (:method ((pool pool) (class symbol) &key %id)
    (get-meme-core pool class
                   :%id %id
                   :memes-slot 'memes)))
