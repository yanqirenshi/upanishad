(in-package :upanishad.pool)

(defun find-meme-core (pool class &key slot value memes-slot)
  (let ((memes (up.pool.core::get-memes-core pool :class class :memes-slot memes-slot)))
    (upanishad.memes:find-meme memes :slot slot :value value)))

(defgeneric find-meme (pool class &key slot value)
  (:documentation "プールからmemeを取得する。get-meme との差別化が。。。")
  (:method (pool class &key slot value)
    (find-meme-core pool class
                    :slot slot
                    :value value
                    :memes-slot 'memes)))
