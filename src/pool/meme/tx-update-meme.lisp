(in-package :upanishad.pool)

(defun slot-value-changed-p (object slot value)
  (or (not (slot-boundp object slot))
      (not (eql (slot-value object slot) value))))

(defun change-slot-value (meme slot value)
  (when (slot-value-changed-p meme slot value)
    (setf (slot-value meme slot) value)
    slot))

(defun update-meme-slots-by-alist (meme alist)
  (if (null alist)
      meme
      (let* ((cons (car alist))
             (changed-slot (change-slot-value meme (car cons) (cdr cons))))
        (if (not changed-slot)
            (update-meme-slots-by-alist meme (cdr alist))
            (cons changed-slot
                  (update-meme-slots-by-alist meme (cdr alist)))))))

(defun update-meme-slots-by-alist (meme alists)
  (when-let ((alist (car alists)))
    (let* ((cons (car alists))
           (changed-slot (change-slot-value meme (car cons) (cdr cons))))
      (if (not changed-slot)
          (update-meme-slots-by-alist meme (cdr alists))
          (cons changed-slot
                (update-meme-slots-by-alist meme (cdr alists)))))))

(defun tx-update-meme-core (pool meme params)
  (let ((changed-slots (update-meme-slots-by-alist meme params)))
    (update-meme-slots-index pool meme changed-slots)))

(defgeneric tx-update-meme (pool meme params)
  (:documentation "meme のスロットの値を変更する。")
  (:method ((pool pool) (meme meme) (params list))
    (tx-update-meme-core pool meme params)))
