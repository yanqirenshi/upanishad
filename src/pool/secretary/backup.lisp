(in-package :upanishad.pool.secretary)

(defun backup-transaction (pool)
  (let* ((timetag (timetag))
         (transaction-log (transaction-log pool))
         (transaction-log-backup (transaction-log-backup-file pool transaction-log timetag)))
    (backup-transaction-log transaction-log transaction-log-backup)))

(defgeneric clear-transaction-log-file-backup (pool)
  (:method ((pool secretary))
    (rm-directory-files (transaction-log-directory-backup pool)
                        (transaction-log-file-extension pool))))
