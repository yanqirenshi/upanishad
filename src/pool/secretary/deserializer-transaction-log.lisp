(in-package :upanishad.pool.secretary)

(defmethod deserializer-transaction-log (pool stream)
  (let ((deserializer (transaction-log-deserializer pool))
        (serialization-state (transaction-log-serialization-state pool)))
    (funcall deserializer stream serialization-state)))
