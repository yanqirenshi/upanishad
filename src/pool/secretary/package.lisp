(in-package :cl-user)
(defpackage :upanishad.pool.secretary
  (:use #:cl
        #:upanishad.persistence)
  (:nicknames :up.pool.secretary)
  (:import-from :upanishad.utilities
                #:copy-file
                #:timetag
                #:rm-directory-files)
  (:export #:secretary
           #:init-secretary
           #:open-transaction-log-stream
           #:close-transaction-log-stream
           #:transaction-log
           #:restore-transaction-log)
  (:export #:backup-transaction)
  (:export #:transaction
           #:make-transaction
           #:execute-on
           #:log-transaction
           #:initiates-rollback
           #:no-rollback-error)
  (:export #:snapshot-transaction-log)
  (:export #:clear-transaction-log-file
           #:clear-transaction-log-file-backup)
  (:documentation ""))
(in-package :upanishad.pool.secretary)
