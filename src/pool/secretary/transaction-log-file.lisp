(in-package :upanishad.pool.secretary)

;;;;;
;;;;; transaction-log file and directory
;;;;;
(defun make-transaction-log-filename (pool &optional suffix)
  (declare (ignore pool))
  (format nil "transaction-log~@[-~a~]" suffix))

(defun make-transaction-log-pathname (pool directory &optional suffix)
  (let ((file-extension (transaction-log-file-extension pool)))
    (merge-pathnames (make-pathname :name (make-transaction-log-filename pool suffix)
                                    :type file-extension)
                     directory)))

;;;;;
;;;;; backup file
;;;;;
(defun transaction-log-backup-file (pool directory timetag)
  (make-transaction-log-pathname pool directory timetag))

(defun snapshot-transaction-log (pool directory timetag)
  (when (probe-file directory)
    (copy-file directory (transaction-log-backup-file pool directory timetag))))

(defun backup-transaction-log (transaction-log transaction-log-backup)
  (when (probe-file transaction-log)
    (copy-file transaction-log transaction-log-backup)))


;;;;;
;;;;;
;;;;;
(defgeneric clear-transaction-log-file (pool)
  (:method ((pool secretary))
    (rm-directory-files (transaction-log-directory pool)
                        (transaction-log-file-extension pool))))
