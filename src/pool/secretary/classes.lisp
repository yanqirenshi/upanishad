(in-package :upanishad.pool.secretary)

(defclass secretary ()
  ((transaction-log-file-extension
    :documentation ""
    :accessor transaction-log-file-extension
    :initarg :transaction-log-file-extension
    :initform "xml"
    :type 'string)
   (transaction-log-directory
    :accessor transaction-log-directory
    :initarg :transaction-log-directory
    :initform nil
    :type 'pathname)
   (transaction-log-directory-backup
    :accessor transaction-log-directory-backup
    :initarg :transaction-log-directory-backup
    :initform nil
    :type 'pathname)
   (transaction-log
    :accessor transaction-log
    :initform nil
    :type 'pathname)
   (transaction-log-stream
    :documentation ":type stream"
    :accessor transaction-log-stream
    :initform nil)
   (transaction-hook
    :documentation ":type function"
    :accessor transaction-hook
    :initarg :transaction-hook
    :initform #'identity)
   (transaction-log-serializer
    :documentation ":type function"
    :accessor transaction-log-serializer
    :initarg :transaction-log-serializer
    :initform #'serialize-xml)
   (transaction-log-serialization-state
    :documentation ":type serialization-state"
    :reader transaction-log-serialization-state
    :initform (make-serialization-state))
   (transaction-log-deserializer
    :documentation ""
    :accessor transaction-log-deserializer
    :initarg :transaction-log-deserializer
    :initform #'deserialize-xml)))

(defclass transaction ()
  ((args
    :documentation ":type cons"
    :accessor args
    :initarg :args
    :initform nil)
   (function
    :documentation ":type symbol"
    :accessor get-function
    :initarg :function
    :initform 'identity))
  (:documentation "A simple Transaction object joining a function and its arguments"))
