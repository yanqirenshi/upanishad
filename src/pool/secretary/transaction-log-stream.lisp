(in-package :upanishad.pool.secretary)

(defgeneric open-transaction-log-stream (secretary)
  (:documentation "")
  (:method ((secretary secretary))
    (with-slots (transaction-log-stream) secretary
      (unless transaction-log-stream
        (setf transaction-log-stream
              (open (transaction-log secretary)
                    :direction :output
                    :if-does-not-exist :create
                    :if-exists :append
                    #+ccl :sharing #+ccl nil))))
    secretary))

(defgeneric close-transaction-log-stream (secretary &key abort)
  (:documentation "Close all open stream associated with secretary (optionally aborting operations in progress)")
  (:method ((secretary secretary) &key abort)
    (with-slots (transaction-log-stream) secretary
      (when transaction-log-stream
        (close transaction-log-stream :abort abort)
        (setf transaction-log-stream nil)))
    secretary))
