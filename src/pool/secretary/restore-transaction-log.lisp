(in-package :upanishad.pool.secretary)

(defun put-error-restore-transaction-log (condition)
  (format *standard-output*
          ";; Warning: error during transaction log restore: ~s~%"
          condition))

(defun restore-transaction-log (pool)
  (let ((transaction-log-file (transaction-log pool))
        (position 0))
    (when (probe-file transaction-log-file)
      (handler-bind ((s-xml:xml-parser-error
                       #'(lambda (condition)
                           (put-error-restore-transaction-log condition)
                           (truncate-file transaction-log-file position)
                           (return-from restore-transaction-log))))
        (with-open-file (in transaction-log-file :direction :input)
          (loop
            (let ((transaction (deserializer-transaction-log pool in)))
              (setf position (file-position in))
              (if transaction
                  (execute-on transaction pool)
                  (return)))))))
    pool))
