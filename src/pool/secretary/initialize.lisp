(in-package :upanishad.pool.secretary)

(defun init-transaction-log-pathname (secretary directory)
  "secretary の transaction-log を初期化します。"
  (setf (transaction-log secretary)
        (make-transaction-log-pathname secretary directory)))

(defun init-secretary (secretary)
  (with-slots (transaction-log-directory) secretary
    (ensure-directories-exist transaction-log-directory)
    (init-transaction-log-pathname secretary transaction-log-directory)
    secretary))
