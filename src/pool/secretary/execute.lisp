(in-package :upanishad.pool.secretary)

(define-condition no-rollback-error (error)
  ()
  (:documentation "Thrown by code inside a transaction to indicate that no rollback is needed"))

(defgeneric initiates-rollback (condition)
  (:documentation "Return true when a condition initiates a rollback when thrown from a transaction")
  (:method ((condition condition)) t)
  (:method ((no-rollback-error no-rollback-error)) nil))

(defgeneric log-transaction (pool transaction)
  (:documentation "Log transaction for pool")
  (:method ((pool secretary) (transaction transaction))
    (let ((out (transaction-log-stream pool))
          (serializer (transaction-log-serializer pool))
          (serialization-state (transaction-log-serialization-state pool)))
      (funcall serializer transaction out serialization-state)
      (terpri out)
      (finish-output out))))

(defmethod log-transaction :after ((pool secretary) (transaction transaction))
  "Execute the transaction-hook"
  (funcall (transaction-hook pool) transaction))

(defmethod execute-on ((transaction transaction) (pool secretary))
  "Execute a transaction itself in the context of a pool"
  (apply (get-function transaction)
         (cons pool (args transaction))))
