(in-package :upanishad.pool)

(defgeneric poolp (pool)
  (:documentation "")
  (:method ((pool pool)) t)
  (:method (other) nil))
