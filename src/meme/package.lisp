(in-package :cl-user)

(defpackage :upanishad.meme
  (:use #:cl
        #:upanishad.atman)
  (:nicknames :up.meme)
  (:export #:%id
           #:meme
           #:blob
           #:name
           #:size
           #:mime-type
           #:keywords)
  (:documentation ""))

(in-package :upanishad.meme)
