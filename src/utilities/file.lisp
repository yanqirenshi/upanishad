(in-package :cl-user)

(defpackage :upanishad.utilities
  (:use #:cl)
  (:nicknames :up.utilities)
  (:export #:copy-file
           #:truncate-file
           #:timetag)
  (:export #:rm-directory-files)
  (:documentation ""))
(in-package :upanishad.utilities)

(defun tmp-pathname (file)
  (merge-pathnames (concatenate 'string "tmp-" (pathname-name file)) file))

(defun truncate-file (file position)
  (let ((tmp-file (tmp-pathname file))
        (buffer (make-string 4096))
        (index 0)
        (read-count 0))
    (with-open-file (in file :direction :input)
      (with-open-file (out tmp-file :direction :output
                                    :if-exists :overwrite
                                    :if-does-not-exist :create)
        (when (> position (file-length in))
          (return-from truncate-file))
        (loop
          (when (= index position) (return))
          (setf read-count (read-sequence buffer in))
          (when (>= (+ index read-count) position)
            (setf read-count (- position index)))
          (incf index read-count)
          (write-sequence buffer out :end read-count))))
    (delete-file file)
    (rename-file tmp-file file))
  (format t ";; Notice: truncated transaction log at position ~d~%" position))

(defun copy-file (source target)
  (let ((buffer (make-string 4096))
        (read-count 0))
    (with-open-file (in source :direction :input)
      (with-open-file (out target :direction :output
                                  :if-exists :overwrite
                                  :if-does-not-exist :create)
        (loop
          (setf read-count (read-sequence buffer in))
          (write-sequence buffer out :end read-count)
          (when (< read-count 4096) (return)))))))

(defun timetag (&optional (universal-time (get-universal-time)))
  (multiple-value-bind (second minute hour date month year)
      (decode-universal-time universal-time 0)
    (format nil
            "~d~2,'0d~2,'0dT~2,'0d~2,'0d~2,'0d"
            year month date hour minute second)))

;;;;;
;;;;; rm-directory-files
;;;;;
(defun directory-files (directory file-extension)
  (directory (merge-pathnames (make-pathname :name :wild :type file-extension) directory)))

(defun rm-directory-files (directory file-extension)
  (when (probe-file directory)
    (dolist (pathname (directory-files directory file-extension))
      (delete-file pathname))))
