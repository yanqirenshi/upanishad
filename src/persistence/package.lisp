(in-package :cl-user)
(defpackage :upanishad.persistence
  (:use :cl)
  (:export #:make-serialization-state
           #:reset-known-slots)
  (:export #:serialize-xml
           #:serializable-slots
           #:deserialize-xml
           #:deserialize-xml-new-element-aux
           #:deserialize-xml-finish-element-aux)
  (:export #:serialize-xml-internal
           #:known-object-id
           #:set-known-object
           #:print-symbol-xml
           #:get-serializable-slots)
  (:export #:serialize-sexp
           #:serialize-sexp-internal
           #:deserialize-sexp
           #:deserialize-sexp-internal)
  (:export #:write-xml-tag-slot
           #:print-symbol-xml)
  (:documentation "XML and s-expression based serialization for Common Lisp and CLOS"))
(in-package :upanishad.persistence)
