(in-package :upanishad.persistence)

(defclass serialization-state ()
  ((xml-parser-state :documentation ""
                     :initform nil)
   (counter :documentation "オブジェクトに割り当てる一意なIDのカウンタ。"
            :accessor get-counter
            :initform 0)
   (hashtable :documentation "オブジェクトと counter で採番した ID を関連付ける連想配列。
キー: オブジェクト(アドレス)、値： counter で採番した ID"
              :reader get-hashtable
              :initform (make-hash-table :test 'eq :size 1024 :rehash-size 2.0))
   (known-slots :documentation "何に使っているんだろう？"
                :initform (make-hash-table)))
  (:documentation "オブジェクトをシリアライズする際、オブジェクトの一意性を保持するためのクラス。"))

(defun make-serialization-state ()
  "Create a reusable serialization state to pass as optional argument to [de]serialize-xml"
  (make-instance 'serialization-state))

(defgeneric reset (serialization-state)
  (:documentation "")
  (:method ((serialization-state serialization-state))
    (with-slots (hashtable counter) serialization-state
      (clrhash hashtable)
      (setf counter 0))))

(defgeneric reset-known-slots (serialization-state &optional class)
  (:documentation "Clear the caching of known slots for class, or for all classes if class is nil")
  (:method ((serialization-state serialization-state) &optional class)
    (with-slots (known-slots) serialization-state
      (if class
          (remhash (if (symbolp class) class (class-name class)) known-slots)
          (clrhash known-slots)))))

(defgeneric known-object-id (serialization-state object)
  (:documentation "")
  (:method ((serialization-state serialization-state) object)
    (gethash object (get-hashtable serialization-state))))

(defgeneric set-known-object (serialization-state object)
  (:documentation "")
  (:method ((serialization-state serialization-state) object)
    (setf (gethash object (get-hashtable serialization-state))
          (incf (get-counter serialization-state)))))

(defgeneric ensure-known-object-id (serialization-state object)
  (:method ((serialization-state serialization-state) object)
    (let ((id (known-object-id serialization-state object)))
      (if id
          (values id nil)
          (values (set-known-object serialization-state object) t)))))
