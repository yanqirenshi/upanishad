(in-package :upanishad.persistence)

(defmacro write-xml-tag-ratio (stream object)
  `(progn
     (write-string "<RATIO>" ,stream)
     (prin1 ,object ,stream)
     (write-string "</RATIO>" ,stream)))
