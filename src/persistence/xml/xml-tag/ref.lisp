(in-package :upanishad.persistence)

(defun write-xml-tag-ref (stream id)
  (write-string "<REF ID=\"" stream)
  (prin1 id stream)
  (write-string "\"/>" stream))
