(in-package :upanishad.persistence)

(defmacro write-xml-tag-complex (stream object)
  `(progn
     (write-string "<COMPLEX>" ,stream)
     (prin1 ,object ,stream)
     (write-string "</COMPLEX>" ,stream)))
