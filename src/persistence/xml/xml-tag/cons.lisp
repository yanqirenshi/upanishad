(in-package :upanishad.persistence)

(defun write-xml-tag-cons-start (stream id)
  (write-string "<CONS ID=\"" stream)
  (prin1 id stream)
  (write-string "\">" stream))

(defun write-xml-tag-cons-end (stream)
  (write-string "</CONS>" stream))

(defun write-xml-tag-cons (stream object id serialization-state)
  (write-xml-tag-cons-start stream id)
  (serialize-xml-internal (car object) stream serialization-state)
  (write-char #\Space stream)
  (serialize-xml-internal (cdr object) stream serialization-state)
  (write-xml-tag-cons-end stream))
