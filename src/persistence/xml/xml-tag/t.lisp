(in-package :upanishad.persistence)

(defmacro write-xml-tag-t (stream)
  `(progn
     (write-string "<TRUE/>" ,stream)))
