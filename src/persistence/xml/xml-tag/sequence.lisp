(in-package :upanishad.persistence)

(defun write-xml-tag-sequence-start (stream object id length)
  (write-string "<SEQUENCE ID=\"" stream)
  (prin1 id stream)
  (write-string "\" CLASS=\"" stream)
  (print-symbol-xml (etypecase object (list 'list) (vector 'vector)) stream)
  (write-string "\" SIZE=\"" stream)
  (prin1 length stream)
  (write-string "\">" stream))

(defun write-xml-tag-sequence-end (stream)
  (write-string "</SEQUENCE>" stream))

(defun write-xml-tag-sequence (stream object id length serialization-state)
  (write-xml-tag-sequence-start stream object id length)
  (map nil
       #'(lambda (element)
           (serialize-xml-internal element stream serialization-state))
       object)
  (write-xml-tag-sequence-end stream))
