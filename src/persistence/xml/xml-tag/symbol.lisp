(in-package :upanishad.persistence)

(defmacro write-xml-tag-symbol (stream object)
  `(progn
     (write-string "<SYMBOL>" ,stream)
     (print-symbol-xml ,object ,stream)
     (write-string "</SYMBOL>" ,stream)))
