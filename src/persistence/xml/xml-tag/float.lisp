(in-package :upanishad.persistence)

(defmacro write-xml-tag-float (stream object)
  `(progn
     (write-string "<FLOAT>" ,stream)
     (prin1 object ,stream)
     (write-string "</FLOAT>" ,stream)))
