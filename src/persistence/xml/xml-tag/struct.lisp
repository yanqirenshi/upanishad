(in-package :upanishad.persistence)

(defun write-xml-tag-struct-start (stream object id)
  (write-string "<STRUCT ID=\"" stream)
  (prin1 id stream)
  (write-string "\" CLASS=\"" stream)
  (print-symbol-xml (class-name (class-of object)) stream)
  (write-string "\">" stream))

(defun write-xml-tag-struct-end (stream)
  (write-string "</STRUCT>" stream))

(defun write-xml-tag-struct (stream object id serialization-state)
  (write-xml-tag-struct-start stream object id)
  (mapc #'(lambda (slot)
            (write-xml-tag-slot stream object slot serialization-state))
        (get-serializable-slots serialization-state object))
  (write-xml-tag-struct-end stream))
