(in-package :upanishad.persistence)

(defun write-xml-tag-object-start (stream object id)
  (write-string "<OBJECT ID=\"" stream)
  (prin1 id stream)
  (write-string "\" CLASS=\"" stream)
  (print-symbol-xml (class-name (class-of object)) stream)
  (princ "\">" stream))

(defun write-xml-tag-object-end (stream)
  (write-string "</OBJECT>" stream))

(defun write-xml-tag-object (stream object id serialization-state)
  (write-xml-tag-object-start stream object id)
  (loop :for slot :in (get-serializable-slots serialization-state object)
        :do (when (slot-boundp object slot)
              (write-xml-tag-slot stream object slot serialization-state)))
  (write-xml-tag-object-end stream))
