(in-package :upanishad.persistence)

;;;;;
;;;;; WRITE-XML-TAG-ENTRY
;;;;;
(defun write-xml-tag-entry (stream key value serialization-state)
  (write-string "<ENTRY><KEY>" stream)
  (serialize-xml-internal key stream serialization-state)
  (write-string "</KEY><VALUE>" stream)
  (serialize-xml-internal value stream serialization-state)
  (princ "</VALUE></ENTRY>" stream))

;;;;;
;;;;; WRITE-XML-TAG-HASH-TABLE
;;;;;
(defun write-xml-tag-hash-table-start (stream object id)
  (write-string "<HASH-TABLE ID=\"" stream)
  (prin1 id stream)
  (write-string "\" TEST=\"" stream)
  (print-symbol-xml (hash-table-test object) stream)
  (write-string "\" SIZE=\"" stream)
  (prin1 (hash-table-size object) stream)
  (write-string "\">" stream))

(defun write-xml-tag-hash-table-end (stream)
  (write-string "</HASH-TABLE>" stream))

(defun write-xml-tag-hash-table (stream object id serialization-state)
  (write-xml-tag-hash-table-start stream object id)
  (maphash #'(lambda (key value)
               (write-xml-tag-entry stream key value serialization-state))
           object)
  (write-xml-tag-hash-table-end stream))
