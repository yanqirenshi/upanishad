(in-package :upanishad.persistence)

(defmacro write-xml-tag-nil (stream)
  `(progn
     (write-string "<NULL/>" ,stream)))
