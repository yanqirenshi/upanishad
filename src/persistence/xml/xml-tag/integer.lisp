(in-package :upanishad.persistence)

(defmacro write-xml-tag-integer (stream object)
  `(progn
     (write-string "<INT>" ,stream)
     (prin1 ,object ,stream)
     (write-string "</INT>" ,stream)))
