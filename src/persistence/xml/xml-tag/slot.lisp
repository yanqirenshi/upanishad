(in-package :upanishad.persistence)

(defun write-xml-tag-slot (stream object slot serialization-state)
  (when (slot-boundp object slot)
    (write-string "<SLOT NAME=\"" stream)
    (print-symbol-xml slot stream)
    (write-string "\">" stream)
    (serialize-xml-internal (slot-value object slot) stream serialization-state)
    (write-string "</SLOT>" stream)))
