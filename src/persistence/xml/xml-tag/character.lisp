(in-package :upanishad.persistence)

(defmacro write-xml-tag-character (stream object)
  `(progn
    (write-string "<CHARACTER>" ,stream)
    (s-xml:print-string-xml (princ-to-string ,object) ,stream)
    (write-string "</CHARACTER>" ,stream)))
