(in-package :upanishad.persistence)

(defmacro write-xml-tag-string (stream object)
  `(progn
     (write-string "<STRING>" ,stream)
     (s-xml:print-string-xml ,object ,stream)
     (write-string "</STRING>" ,stream)))
