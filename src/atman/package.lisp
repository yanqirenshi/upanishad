(in-package :cl-user)
(defpackage :upanishad.atman
  (:nicknames :up.atman)
  (:use #:cl)
  (:export #:brahman
           #:atman
           #:%id)
  (:documentation ""))
(in-package :upanishad.atman)
