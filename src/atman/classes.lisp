(in-package :upanishad.atman)

(defclass brahman () ()
  (:documentation "思想的/象徴的なクラス。今んところ意味はないけぇ。"))

(defclass atman (brahman)
  ((%id :documentation "Return an external, unique, immutable identifier for object (typically an integer)"
        :reader %id
        :initarg :%id
        :initform -1))
  (:documentation "Superclass for objects with an id"))
