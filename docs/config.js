const _CONFIG = {
    api : {
        scheme: 'http',
        host: 'renshi',
        port: '80',
        path: {
            // prefix: '/upanishad/api/v1'
            prefix: '/upanishad'
        }
    }
};
