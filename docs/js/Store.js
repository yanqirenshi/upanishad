class Store extends Vanilla_Redux_Store {
    constructor(reducer) {
        super(reducer, Immutable.Map({}));
    }
    childPagePackagess () {
        return {
            code: "packages",
            children: [
                {
                    code: "package",
                    regex: /^\d+$/,
                    tag: (node, route) => {
                        return 'package-' + route.reverse()[0];
                    }
                },
            ]
        };
    }
    childPageClasses () {
        return {
            code: "classes",
            children: [
                {
                    code: "class",
                    regex: /^\d+$/,
                    tag: (node, route) => {
                        return 'class-' + route.reverse()[0];
                    }
                },
            ]
        };
    }
    childPageVariables () {
        return {
            code: "variables",
            children: [
                {
                    code: "variable",
                    regex: /^\d+$/,
                    tag: (node, route) => {
                        return 'variable-' + route.reverse()[0];
                    }
                },
            ]
        };
    }
    childPageOperators () {
        return {
            code: "operators",
            children: [
                {
                    code: "operator",
                    regex: /^\d+$/,
                    tag: (node, route) => {
                        return 'operator-' + route.reverse()[0];
                    }
                },
            ]
        };
    }
    page_home () {
        return {
            menu_label: 'Home',
            code: "home",
            tag: 'page-home',
            children: [
                this.childPagePackagess(),
                this.childPageVariables(),
                this.childPageClasses(),
                this.childPageOperators(),
            ]
        };
    }
    page_usage () {
        return {
            menu_label: 'Usage',
            code: "usage",
            tag: 'usage-sec_root',
        };
    }
    page_upanishad () {
        return {
            menu_label: 'ウパニシャッド',
            code: "upanishad",
            tag: 'package_upanishad_sec-root',
        };
    }
    page_upanishadIndex () {
        return {
            menu_label: 'インデックス',
            code: "upanishad-index",
            tag: 'package_upanishad-index_sec-root',
        };
    }
    page_upanishadMeme () {
        return {
            menu_label: '情報遺伝子',
            code: "upanishad-meme",
            tag: 'package_upanishad-meme_sec-root',
        };
    }
    page_upanishadPool () {
        return {
            menu_label: 'プール',
            code: "upanishad-pool",
            tag: 'package_upanishad-pool_sec-root',
            children: [
                { code: 'start',           tag: 'defgeneric-function_pool_start'},
                { code: 'stop',            tag: 'defgeneric-function_pool_stop'},
                { code: 'snapshot',        tag: 'defgeneric-function_pool_snapshot'},
                { code: 'restore',         tag: 'defgeneric-function_pool_restore'},
                { code: 'totally-destroy', tag: 'defgeneric-function_pool_totally-destroy'},
                { code: 'tx-create-meme',  tag: 'defgeneric-function_pool_tx-create-meme'},
                { code: 'tx-update-meme',  tag: 'defgeneric-function_pool_tx-update-meme'},
                { code: 'tx-delete-meme',  tag: 'defgeneric-function_pool_tx-delete-meme'},
                { code: 'find-meme',       tag: 'defgeneric-function_pool_find-meme'},
                { code: 'get-meme',        tag: 'defgeneric-function_pool_get-meme'},
                { code: 'get-index',       tag: 'defgeneric-function_pool-core_get-index'},
                { code: 'tx-add-index',    tag: 'defgeneric-function_pool-core_tx-add-index'},
                { code: 'tx-remove-index', tag: 'defgeneric-function_pool-core_tx-remove-index'},
            ],
        };
    }
    page_upanishadPoolCore () {
        return {
            menu_label: '核',
            code: "upanishad-pool-core",
            tag: 'package_upanishad-pool-core_sec-root',
            children: [
                { code: 'get-index',       tag: 'defgeneric-function_pool-core_get-index' },
                { code: 'tx-add-index',    tag: 'defgeneric-function_pool-core_tx-add-index' },
                { code: 'tx-remove-index', tag: 'defgeneric-function_pool-core_tx-remove-index' },
            ],
        };
    }
    page_persistence () {
        return {
            menu_label: '永続化',
            code: "persistence",
            tag: 'persistence-sec_root',
            children: [
                { code: 'serialize-xml-internal', tag: 'upanishad_persistence_serialize-xml-internal'},
                { code: 'deserialize-xml-new-element', tag: 'upanishad_persistence_deserialize-xml-new-element'},
                { code: 'deserialize-xml-new-element-aux', tag: 'upanishad_persistence_deserialize-xml-new-element-aux'},
                { code: 'deserialize-xml-finish-element', tag: 'upanishad_persistence_deserialize-xml-finish-element'},
                { code: 'deserialize-xml-finish-element-aux', tag: 'upanishad_persistence_deserialize-xml-finish-element-aux'},
            ],
            stye: {
                color: { 1: '#fdeff2', 2: '#e0e0e0', 3: '#e198b4', 4: '#ffffff', 5: '#eeeeee', 5: '#333333' }
            }
        };
    }
    pages () {
        return [
            this.page_home(),
            this.page_usage(),
            this.page_upanishad(),
            this.page_upanishadMeme(),
            this.page_upanishadPool(),
            this.page_upanishadIndex(),
            {
                menu_label: '図書館員',
                code: "upanishad-pool-librarian",
                tag: 'package_upanishad-pool-librarian_sec-root',
            },
            {
                menu_label: '書記',
                code: "upanishad-pool-secretary",
                tag: 'package_upanishad-pool-secretary_sec-root',
            },
            this.page_persistence(),
        ];
    }
    init () {
        let data = {
            site: {
                active_page: 'home',
                home_page: 'home',
                pages: this.pages()
            },
            data: {
                nodes: { ht: {}, list: [] },
                edges: { ht: {}, list: [] },
                indexes: {
                    edge: {
                        from: {},
                        to: {},
                    }
                }
            }
        };

        for (var i in data.site.pages) {
            let page = data.site.pages[i];
            for (var k in page.sections) {
                let section = page.sections[k];
                let hash = '#' + page.code;

                if (section.code!='root')
                    hash += '/' + section.code;

                section.hash = hash;
            }
        }


        this._contents = Immutable.Map(data);
        return this;
    }
}
