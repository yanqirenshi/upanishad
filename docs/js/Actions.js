class Actions extends Vanilla_Redux_Actions {
    movePage (data) {
        let state = STORE.get('site');

        // Page は選択された route の根なので "[0]" を指定。
        state.active_page = data.route[0];

        STORE.dispatch({
            type: 'MOVE-PAGE',
            data: { site: state },
            route: data.route,
        });
    }
    /* **************************************************************** *
     *
     *  Utility
     *
     * **************************************************************** */
    mergeList2pool (list, pool) {
        let new_pool = {
            ht: Object.assign({}, pool.ht),
            list: pool.list.slice(),
        };

        for (let obj of list) {
            if (new_pool[obj.id])
                continue;

            new_pool.ht[obj.id] = obj;
            new_pool.list.push(obj);
        }

        return new_pool;
    }
    /* **************************************************************** *
     *
     *
     * **************************************************************** */
    fetchNodes () {
        API.get('/data/nodes.json', function (response) {
            STORE.dispatch(this.fetchedNodes(response));
        }.bind(this));
    }
    fetchedNodes (response) {
        let state = STORE.get('data');

        state.nodes = this.mergeList2pool(response, state.nodes);

        return {
            type: 'FETCHED-NODES',
            data: { data: state },
        };
    }
    fetchEdges () {
        API.get('/data/edges.json', function (response) {
            STORE.dispatch(this.fetchedEdges(response));
        }.bind(this));
    }
    fetchedEdges (response) {
        let state = STORE.get('data');

        state.edges = this.mergeList2pool(response, state.edges);

        for (let edge of state.edges.list) {
            let from_node = state.nodes.ht[edge.from];
            let to_node = state.nodes.ht[edge.to];

            edge.from = from_node;
            edge.to = to_node;
        }

        // create index
        for (let edge of state.edges.list) {
            let from_id = edge.from.id;
            let to_id   = edge.to.id;

            // init index
            if (!state.indexes.edge.from[from_id])
                state.indexes.edge.from[from_id] = { nodes: {}, edges: {} };

            if (!state.indexes.edge.to[to_id])
                state.indexes.edge.to[to_id] = { nodes: {}, edges: {} };

            // set edge to index
            state.indexes.edge.from[from_id].edges[edge.id] = edge;
            state.indexes.edge.to[to_id].edges[edge.id] = edge;

            // set node to index
            let from_node = edge.from;
            let to_node   = edge.to;

            state.indexes.edge.from[from_id].nodes[edge.id] = from_node;
            state.indexes.edge.to[to_id].nodes[edge.id]     = to_node;
        }



        return {
            type: 'FETCHED-EDGES',
            data: {
                data: state
            },
        };
    }
    finishedFirstLoad () {
        STORE.dispatch({
            type: 'FINISHED-FIRST-LOAD',
        });
    }
}
