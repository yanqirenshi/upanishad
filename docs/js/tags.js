riot.tag2('app-global-menu-bland', '<div> <div> UP </div> </div>', 'app-global-menu-bland > div { display: flex; justify-content: center; align-items: center; margin-top: 22px; margin-bottom: 22px; } app-global-menu-bland > div > div{ display: flex; justify-content: center; align-items: center; width:33px; height:33px; border-radius: 3px; font-weight: bold; background: #BFA742; color: #830411; }', '', function(opts) {
});

riot.tag2('app-global-menu-item', '<div class="menu-item-root {isActive()}"> <div class="menu-item-text" code="{opts.source.code}" onclick="{clickItem}"> {opts.source.menu_label} </div> </div>', 'app-global-menu-item .menu-item-root { display: block; word-break: keep-all; margin-bottom: 3px; padding-left: 11px; padding-right: 11px; } app-global-menu-item .menu-item-text { padding: 6px 8px; border-radius: 3px; display: flex; justify-content: center; align-items: center; font-weight: bold; color: #BFA742; background: #830411; } app-global-menu-item .menu-item-text:hover { color: #F7DE59; } app-global-menu-item .menu-item-root.active .menu-item-text { color: #830411; background: #F7DE59; }', '', function(opts) {
     this.clickItem = (e) => {
         let code = e.target.getAttribute('code');

         location.hash = '#' + code;
     };
     this.isActive = (obj) => {
         return this.opts.active_page == opts.source.code ? 'active' : '';
     };
});

riot.tag2('app-global-menu-switcher', '<div> <div> <p></p> </div> <div> <p> < </p> </div> </div>', 'app-global-menu-switcher > div { display: flex; color: #BFA742; border-top: 1px solid #BFA742; } app-global-menu-switcher > div > div { padding: 8px; } app-global-menu-switcher > div > div:first-child { flex-grow: 1; } app-global-menu-switcher > div > div:last-child { border-left: 1px solid #BFA742; font-weight: bold; } app-global-menu-switcher > div > div:last-child:hover { color: #830411; background: #BFA742; }', '', function(opts) {
});

riot.tag2('app-global-menu', '<div ref="root"> <app-global-menu-bland></app-global-menu-bland> <app-global-menu-item each="{obj in list()}" source="{obj}" active_page="{activePage()}"></app-global-menu-item> <div style="flex-grow:1;"></div> <app-global-menu-switcher></app-global-menu-switcher> </div>', 'app-global-menu { display: block; } app-global-menu > div { position: fixed; height: 100vh; display: flex; flex-direction: column; background: #830411; }', '', function(opts) {
     this.on('update', () => {
         let w = this.refs.root.clientWidth;

         this.root.style.width = w + 'px'
     });
     this.on('mount', () => {
         let w = this.refs.root.clientWidth;

         this.root.style.width = w + 'px'
     });

     this.list = () => {
         return STORE.get('site.pages');
     };
     this.activePage = () => {
         return STORE.get('site.active_page');
     };
});

riot.tag2('app-page-area', '', '', '', function(opts) {
     this.draw = () => {
         if (this.opts.route)
             ROUTER.draw(this, STORE.get('site.pages'), this.opts.route);
     }
     this.on('mount', () => {
         this.draw();
     });
     this.on('update', () => {
         this.draw();
     });
});

riot.tag2('app', '<div class="flex-root"> <div class="flex-left"> <app-global-menu></app-global-menu> </div> <div class="flex-right"> <app-page-area></app-page-area> </div> </div>', 'app > .flex-root { display: flex; width: 100vw; height: 100vh; } app > .flex-root > .flex-left { } app > .flex-root > .flex-right { flex-grow: 1; }', '', function(opts) {
     this.site = () => {
         return STORE.state().get('site');
     };
     this.updateMenuBar = () => {
         if (this.tags['menu-bar'])
             this.tags['menu-bar'].update();
     }
     STORE.subscribe((action)=>{
         if (action.type=='MOVE-PAGE') {
             this.updateMenuBar();

             this.tags['app-global-menu'].update();
             this.tags['app-page-area'].update({ opts: { route: action.route }});
         }
     })

     this.on('mount', () => {
         let route = location.hash.substring(1).split('/');

         ACTIONS.movePage({ route: route });
     });

     window.addEventListener('resize', (event) => {
         this.update();
     });

     if (location.hash=='')
         location.hash='#home'
});

riot.tag2('class-1000', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1001', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1002', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1003', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1004', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1005', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1006', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1007', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1008', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1009', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1010', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1011', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1012', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1013', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1014', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1015', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1016', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1017', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-1018', '<class-header></class-header> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Class Precedence List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Slot List</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents"> </div> </div> </section> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('class-header', '<section class="hero"> <div class="hero-body up-header"> <div class="container"> <h1 class="title">Class: source.name</h1> <h2 class="subtitle">YYY</h2> </div> </div> </section>', '', '', function(opts) {
     this.dataID = () => {
         return location.hash.split('/').reverse()[0] * 1;
     };
     this.loadSource = () => {
         return STORE.get('data.nodes.ht')[this.dataID()];
     }

     this.source = null;
     this.on('before-mount', () => {
         this.source = this.loadSource();
     });
     this.on('update', () => {
         this.source = this.loadSource();
     });
});

riot.tag2('menu-bar', '<aside class="menu"> <p ref="brand" class="menu-label" onclick="{clickBrand}"> {opts.brand.label} </p> <ul class="menu-list"> <li each="{opts.site.pages}"> <a class="{opts.site.active_page==code ? \'is-active\' : \'\'}" href="{\'#\' + code}"> {menu_label} </a> </li> </ul> </aside> <div class="move-page-menu hide" ref="move-panel"> <p each="{moves()}"> <a href="{href}">{label}</a> </p> </div>', 'menu-bar .move-page-menu { z-index: 666665; background: #ffffff; position: fixed; left: 55px; top: 0px; min-width: 111px; height: 100vh; box-shadow: 2px 0px 8px 0px #e0e0e0; padding: 22px 55px 22px 22px; } menu-bar .move-page-menu.hide { display: none; } menu-bar .move-page-menu > p { margin-bottom: 11px; } menu-bar > .menu { z-index: 666666; height: 100vh; width: 55px; padding: 11px 0px 11px 11px; position: fixed; left: 0px; top: 0px; background: #830411; } menu-bar .menu-label, menu-bar .menu-list a { padding: 0; width: 33px; height: 33px; text-align: center; margin-top: 8px; border-radius: 3px; background: none; color: #BFA742; font-weight: bold; padding-top: 7px; font-size: 14px; } menu-bar .menu-label,[data-is="menu-bar"] .menu-label{ background: #830411; color: #FFDE59; } menu-bar .menu-label.open,[data-is="menu-bar"] .menu-label.open{ background: #ffffff; color: #830411; width: 44px; border-radius: 3px 0px 0px 3px; text-shadow: 0px 0px 1px #eee; padding-right: 11px; } menu-bar .menu-list a.is-active { width: 33px; border-radius: 3px 3px 3px 3px; background: #FFDE59; color: #333333; }', '', function(opts) {
     this.moves = () => {
         let moves = [
         ]
         return moves.filter((d)=>{
             return d.code != this.opts.current;
         });
     };

     this.brandStatus = (status) => {
         let brand = this.refs['brand'];
         let classes = brand.getAttribute('class').trim().split(' ');

         if (status=='open') {
             if (classes.find((d)=>{ return d!='open'; }))
                 classes.push('open')
         } else {
             if (classes.find((d)=>{ return d=='open'; }))
                 classes = classes.filter((d)=>{ return d!='open'; });
         }
         brand.setAttribute('class', classes.join(' '));
     }

     this.clickBrand = () => {
         return;

         let panel = this.refs['move-panel'];
         let classes = panel.getAttribute('class').trim().split(' ');

         if (classes.find((d)=>{ return d=='hide'; })) {
             classes = classes.filter((d)=>{ return d!='hide'; });
             this.brandStatus('open');
         } else {
             classes.push('hide');
             this.brandStatus('close');
         }
         panel.setAttribute('class', classes.join(' '));
     };
});

riot.tag2('page-tabs', '<div class="tabs is-{type()} is-small"> <ul> <li each="{opts.core.tabs}" class="{opts.core.active_tab==code ? \'is-active\' : \'\'}"> <a code="{code}" onclick="{clickTab}">{label}</a> </li> </ul> </div>', 'page-tabs .is-boxed li:first-child { margin-left: 22px; } page-tabs .is-toggle li a { background: #ffffff; } page-tabs .tabs.is-toggle li.is-active a { background-color: RGB(254, 242, 99); border-color: RGB(254, 242, 99); font-weight: bold; }', '', function(opts) {
     this.clickTab = (e) => {
         let code = e.target.getAttribute('code');
         this.opts.callback(e, 'CLICK-TAB', { code: code });
     };
     this.type = () => {
         return this.opts.type ? this.opts.type : 'boxed';
     };
});

riot.tag2('section-breadcrumb', '<section-container data="{path()}"> <nav class="breadcrumb" aria-label="breadcrumbs"> <ul> <li each="{opts.data}"> <a class="{active ? \'is-active\' : \'\'}" href="{href}" aria-current="page">{label}</a> </li> </ul> </nav> </section-container>', 'section-breadcrumb section-container > .section,[data-is="section-breadcrumb"] section-container > .section{ padding-top: 3px; }', '', function(opts) {
     this.path = () => {
         let hash = location.hash;
         let path = hash.split('/');

         if (path[0] && path[0].substr(0,1)=='#')
             path[0] = path[0].substr(1);

         let out = [];
         let len = path.length;
         let href = null;
         for (var i in path) {
             href = href ? href + '/' + path[i] : '#' + path[i];

             if (i==len-1)
                 out.push({
                     label: path[i],
                     href: hash,
                     active: true
                 });

             else
                 out.push({
                     label: path[i],
                     href: href,
                     active: false
                 });
         }
         return out;
     }
});

riot.tag2('section-container', '<section class="section"> <div class="container"> <h1 class="title is-{opts.no ? opts.no : 3}"> {opts.title} </h1> <h2 class="subtitle">{opts.subtitle}</h2> <yield></yield> </div> </section>', '', '', function(opts) {
});

riot.tag2('section-contents', '<section class="section"> <div class="container"> <h1 class="title is-{opts.no ? opts.no : 3}"> {opts.title} </h1> <h2 class="subtitle">{opts.subtitle}</h2> <div class="contents"> <yield></yield> </div> </div> </section>', 'section-contents > section.section { padding: 0.0rem 1.5rem 2.0rem 1.5rem; }', '', function(opts) {
});

riot.tag2('section-footer', '<footer class="footer"> <div class="container"> <div class="content has-text-centered"> <p> </p> </div> </div> </footer>', 'section-footer > .footer { padding-top: 13px; padding-bottom: 13px; height: 66px; background: #fef4f4; opacity: 0.7; }', '', function(opts) {
});

riot.tag2('section-header-with-breadcrumb', '<section-header title="{opts.title}"></section-header> <section-breadcrumb></section-breadcrumb>', 'section-header-with-breadcrumb section-header > .section,[data-is="section-header-with-breadcrumb"] section-header > .section{ margin-bottom: 3px; }', '', function(opts) {
});

riot.tag2('section-header', '<section class="section"> <div class="container"> <h1 class="title is-{opts.no ? opts.no : 1}"> {opts.title} </h1> <h2 class="subtitle">{opts.subtitle}</h2> <yield></yield> </div> </section>', 'section-header > .section { background: #830411; margin-bottom: 33px; } section-header .title, section-header .subtitle { color: #BFA742; }', '', function(opts) {
});

riot.tag2('section-list', '<table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <th>機能</th> <th>概要</th> </tr> </thead> <tbody> <tr each="{data()}"> <td><a href="{hash}">{title}</a></td> <td>{description}</td> </tr> </tbody> </table>', '', '', function(opts) {
     this.data = () => {
         return opts.data.filter((d) => {
             if (d.code=='root') return false;

             let len = d.code.length;
             let suffix = d.code.substr(len-5);
             if (suffix=='_root' || suffix=='-root')
                 return false;

             return true;
         });
     };
});

riot.tag2('operators', '<table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <th>Type</th> <th>Name</th> <th>Description</th> </tr> </thead> <tbody> <tr each="{opts.data}"> <td>{type}</td> <td><a href="#upanishad-pool/{code}">{code.toUpperCase()}</a></td> <td>{description}</td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('sections-list', '<table class="table"> <tbody> <tr each="{opts.data}"> <td><a href="{hash}">{title}</a></td> </tr> </tbody> </table>', '', '', function(opts) {
});

riot.tag2('symbol-syntax', '<div class="syntax"> <b style="color: #888888;">{opts.name}</b> <i>{opts.args}</i> <span class="{diplay(\'keys\')}" style="color: #888888;">&key</span> <i class="{diplay(\'keys\')}">{opts.keys}</i> <span class="{diplay(\'optional\')}" style="color: #888888;">&optional</span> <i class="{diplay(\'optional\')}">{opts.optional}</i> <span style="color: #888888;">&rArr;</span> <i>{opts.results}</i> </div>', 'symbol-syntax .hiden { display: none; } symbol-syntax .syntax > * { margin-right: 8px; }', '', function(opts) {
     this.diplay = (code) => {
         return opts[code] ? '' : 'hiden';
     }
});

riot.tag2('dependencies', '<section class="section"> <div class="container"> <h1 class="title">Dependencies</h1> <h2 class="subtitle"></h2> <div class="contents"> <table class="table is-bordered is-striped is-narrow is-hoverable"> <thead> <tr> <th rowspan="2">ID</th> <th colspan="3">From</th> <th rowspan="2">Type</th> <th colspan="3">From</th> </tr> <tr> <th>ID</th> <th>Name</th> <th>Class</th> <th>ID</th> <th>Name</th> <th>Class</th> </tr> </thead> <tbody> <tr each="{obj in list()}"> <td>{obj.id}</td> <td>{obj.from.id}</td> <td>{obj.from.name}</td> <td>{obj.from._class}</td> <td>{obj.type}</td> <td>{obj.to.id}</td> <td>{obj.to.name}</td> <td>{obj.to._class}</td> </tr> </tbody> </table> </div> </div> </section>', '', '', function(opts) {
     this.dataID = () => {
         return location.hash.split('/').reverse()[0]*1;
     };
     this.list = () => {
         let index = STORE.get('data.indexes.edge')
         let node_id = this.dataID();

         let edges = {};

         this.mergeEdges = (index) => {
             if (!index || !index.edges)
                 return;

             let ht = index.edges;
             for (let key in ht)
                 edges[key] = ht[key];
         };

         this.mergeEdges(index.from[node_id]);
         this.mergeEdges(index.to[node_id]);

         let list = [];
         for (let key in edges)
             list.push(edges[key]);

         return list;
     }
});

riot.tag2('class-100', '<class-header></class-header>', '', '', function(opts) {
});

riot.tag2('class-header', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title">Class: source.name</h1> <h2 class="subtitle">YYY</h2> </div> </div> </section>', '', '', function(opts) {
     this.dataID = () => {
         return location.hash.split('/').reverse()[0] * 1;
     };
     this.loadSource = () => {
         return STORE.get('data.nodes.ht')[this.dataID()];
     }

     this.source = null;
     this.on('before-mount', () => {
         this.source = this.loadSource();
     });
     this.on('update', () => {
         this.source = this.loadSource();
     });
});

riot.tag2('package-1', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-10', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-11', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-12', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-13', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-14', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-15', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-2', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-3', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-4', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-5', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-6', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-7', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-8', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-9', '<package-header></package-header> <dependencies></dependencies>', '', '', function(opts) {
});

riot.tag2('package-header', '<section class="hero up-header"> <div class="hero-body"> <div class="container"> <h1 class="title">Package: source.name</h1> <h2 class="subtitle">YYY</h2> </div> </div> </section>', '', '', function(opts) {
     this.dataID = () => {
         return location.hash.split('/').reverse()[0] * 1;
     };
     this.loadSource = () => {
         return STORE.get('data.nodes.ht')[this.dataID()];
     }

     this.source = null;
     this.on('before-mount', () => {
         this.source = this.loadSource();
     });
     this.on('update', () => {
         this.source = this.loadSource();
     });
});

riot.tag2('page-home', '<div class="page-root"> <section-header title="Upanishad"></section-header> <div> <page-tabs core="{page_tabs}" callback="{clickTab}"></page-tabs> </div> <div class="tab-contents"> <page-home_tab-ovrview class="hide"></page-home_tab-ovrview> <page-home_tab-variables class="hide"></page-home_tab-variables> <page-home_tab-packages class="hide"></page-home_tab-packages> <page-home_tab-classes class="hide"></page-home_tab-classes> <page-home_tab-operators class="hide"></page-home_tab-operators> </div> </div>', '', '', function(opts) {
     this.page_tabs = new PageTabs([
         { code: 'ovrview',   label: 'Ovrview',   tag: 'page-home_tab-ovrview' },
         { code: 'packages',  label: 'Packages',  tag: 'page-home_tab-packages' },
         { code: 'variables', label: 'Variables', tag: 'page-home_tab-variables' },
         { code: 'classes',   label: 'Classes',   tag: 'page-home_tab-classes' },
         { code: 'operators', label: 'Operators', tag: 'page-home_tab-operators' },
     ]);

     this.on('mount', () => {

         let len = Object.keys(this.tags).length;
         if (len==0)
             return;

         this.page_tabs.switchTab(this.tags)
         this.update();
     });

     this.clickTab = (e, action, data) => {
         if (this.page_tabs.switchTab(this.tags, data.code))
             this.update();
     };
});

riot.tag2('page-home_tab-classes', '<section class="section"> <div class="container"> <h1 class="title"></h1> <h2 class="subtitle"></h2> <div class="contents"> <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth"> <thead> <tr> <th>ID</th> <th>Name</th> <th>Description</th> <th>File</th> </tr> </thead> <tbody> <tr each="{obj in list()}"> <td> <a href="{idLink(obj.id)}"> {obj.id} </a> </td> <td>{obj.name}</td> <td>{obj.description}</td> <td> <a href="{sourceLink(obj.file)}"> {obj.file} </a> </td> </tr> </tbody> </table> </div> </div> </section>', '', '', function(opts) {
     this.list = () => {
         return STORE.get('data.nodes.list').filter((d) => {
             return d._class == 'CL-CLASS';
         });
     }
     this.idLink = (v) => {
         return location.hash + '/classes/' + v;
     };
     this.sourceLink = (v) => {
         let head = 'https://gitlab.com/yanqirenshi/upanishad/blob/master/src/';

         return head + v;
     };
});

riot.tag2('page-home_tab-operators', '', '', '', function(opts) {
});

riot.tag2('page-home_tab-ovrview', '<section class="section"> <div class="container"> <h1 class="title"></h1> <h2 class="subtitle"></h2> <div class="contents"> <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth"> <thead> <tr> <th>Name</th> <th>Package</th> <th>Description</th> </tr> </thead> <tbody> <tr each="{obj in list()}"> <td>{obj.name}</td> <td>{obj.package}</td> <td>{obj.description}</td> </tr> </tbody> </table> </div> </div> </section>', '', '', function(opts) {
     this.list = () => {
         return [
             { name: '情報遺伝子',   package: 'meme',         description: '' },
             { name: 'プール',       package: 'pool',         description: '' },
             { name: 'インデックス', package: 'index',        description: '' },
             { name: '書記',         package: 'perpetuation', description: '' },
             { name: '図書館員',     package: 'librarian',    description: '' },
             { name: '永続',         package: 'persistence',  description: '' },
         ];
     };
});

riot.tag2('page-home_tab-packages', '<section class="section"> <div class="container"> <h1 class="title"></h1> <h2 class="subtitle"></h2> <div class="contents"> <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth"> <thead> <tr> <th>ID</th> <th>Name</th> <th>Description</th> <th>File</th> </tr> </thead> <tbody> <tr each="{obj in list()}"> <td> <a href="{idLink(obj.id)}"> {obj.id} </a> </td> <td>{obj.name}</td> <td>{obj.description}</td> <td> <a href="{sourceLink(obj.file)}"> {obj.file} </a> </td> </tr> </tbody> </table> </div> </div> </section>', '', '', function(opts) {
     this.list = () => {
         return STORE.get('data.nodes.list').filter((d) => {
             return d._class == 'CL-PACKAGE';
         });
     }
     this.idLink = (v) => {
         return location.hash + '/packages/' + v;
     };
     this.sourceLink = (v) => {
         let head = 'https://gitlab.com/yanqirenshi/upanishad/blob/master/src/';

         return head + v;
     };
});

riot.tag2('page-home_tab-procedures', '', '', '', function(opts) {
});

riot.tag2('page-home_tab-variables', '<section class="section"> <div class="container"> <h1 class="title"></h1> <h2 class="subtitle"></h2> <div class="contents"> <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth"> <thead> <tr> <th>ID</th> <th>Name</th> <th>Description</th> <th>File</th> </tr> </thead> <tbody> <tr each="{obj in list()}"> <td> <a href="{idLink(obj.id)}"> {obj.id} </a> </td> <td>{obj.name}</td> <td>{obj.description}</td> <td> <a href="{sourceLink(obj.file)}"> {obj.file} </a> </td> </tr> </tbody> </table> </div> </div> </section>', '', '', function(opts) {
     this.list = () => {
         return STORE.get('data.nodes.list').filter((d) => {
             return d._class == 'CL-VARIABLE';
         });
     }
     this.idLink = (v) => {
         return location.hash + '/classes/' + v;
     };
     this.sourceLink = (v) => {
         let head = 'https://gitlab.com/yanqirenshi/upanishad/blob/master/src/';

         return head + v;
     };
});

riot.tag2('package_upanishad-index_sec-root', '<section-header title="Package: UPANISHAD.INDEX"></section-header> <section-container title="概要"> </section-container> <section-container title="CLASSES"> <section-contents title="CLASS図"> <p> <pre style="font-size:12px; line-height:12px;">\n+-------+\n| index |\n|=======|\n+-------+\n    |\n    V\n+--------------+\n| slot-index   |\n|==============|\n| class-symbol |\n| slot-symbol  |\n| %id->value   |\n+--------------+\n    |\n    +---------------------------------+\n    |                                 |\n    V                                 V\n+-------------------+       +---------------------+\n| slot-index-unique |       | slot-index-multiple |\n|===================|       |=====================|\n| value->object     |       | value->objects      |\n+-------------------+       +---------------------+\n                </pre> </p> </section-contents> </section-container> <section-container title="Operators"> <section-contents title="一覧"> <p> <pre style="font-size:12px; line-height:12px;">\n| file                     | type             | symbol                       | args                                            | package         |\n|--------------------------+------------------+------------------------------+-------------------------------------------------+-----------------|\n| slot-index-multiple.lisp | function         | ensure-%id->object           | value->objects value                            | upanishad.index |\n|                          | function         | slot-index-multiple-contexts | index meme                                      | upanishad.index |\n|                          | function         | add-object-add-multi         | meme value %id %id->object %id->value           | upanishad.index |\n|                          | generic function | add-object                   | index meme                                      | upanishad.index |\n|                          | generic function | add-objects                  | index memes                                     | upanishad.index |\n|                          | function         | add-object-remove-multi      | value %id %id->object %id->value value->objects | upanishad.index |\n|                          | generic function | remove-object                | index meme                                      | upanishad.index |\n|                          | generic function | change-object                | index meme                                      | upanishad.index |\n|                          | generic function | get-at-value                 | index value                                     | upanishad.index |\n| slot-index-unique.lisp   | generic function | add-object                   | index meme                                      | upanishad.index |\n|                          | generic function | add-objects                  | index memes                                     | upanishad.index |\n|                          | generic function | remove-object                | index meme                                      | upanishad.index |\n|                          | function         | change-object-remove         | index value meme                                | upanishad.index |\n|                          | function         | change-object-add            | index value meme                                | upanishad.index |\n|                          | generic function | change-object                | index meme                                      | upanishad.index |\n|                          | generic function | get-at-value                 | index value                                     | upanishad.index |\n| slot-index.lisp          | generic function | get-index-key                | slot-index                                      | upanishad.index |\n|                          | function         | make-slot-index              | class-symbol slot-symbol type &optional objects | upanishad.index |\n| utility.lisp             | function         | get-slot-index-class         | type                                            | upanishad.index |\n|                          | function         | assert-class                 | class meme                                      | upanishad.index |\n                </pre> </p> </section-contents> </section-container>', '', '', function(opts) {
});

riot.tag2('upanishad-index', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('package_upanishad-meme_sec-root', '<section-header title="Package: UPANISHAD.MEME"></section-header> <section-container title="概要"> </section-container> <section-container title="CLASSES"> <section-contents title="CLASS図"> <p> <pre style="font-size:12px; line-height:12px;">\n+---------+\n| brahman |\n|=========|\n+---------+\n    |\n    V\n+-------+\n| atman |\n|=======|\n| %id   | integer\n+-------+\n    |\n    V\n+------+\n| meme |\n|======|\n+------+\n    |\n    V\n+-----------+\n| blob      |\n|===========|\n| name      |\n| size      |\n| mime-type |\n| keywords  |\n+-----------+\n                </pre> </p> </section-contents> </section-container>', '', '', function(opts) {
});

riot.tag2('upanishad-meme', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('class_pool-deserialize', '<section-container title="Class: POOL-DESERIALIZE"></section-container>', '', '', function(opts) {
});

riot.tag2('class_pool-serialize', '<section-container title="Class: POOL-SERIALIZE"></section-container>', '', '', function(opts) {
});

riot.tag2('class_pool-snapshot', '<section-container title="Class: POOL-SNAPSHOT"></section-container>', '', '', function(opts) {
});

riot.tag2('class_pool-transaction', '<section-container title="Class: POOL-TRANSACTION"></section-container>', '', '', function(opts) {
});

riot.tag2('class_pool', '<section-container title="Class: POOL"></section-container>', '', '', function(opts) {
});

riot.tag2('package_upanishad-pool_sec-root', '<section-header title="Package: UPANISHAD.POOL"></section-header> <package_upanishad-pool_sec-root_operators></package_upanishad-pool_sec-root_operators> <package_upanishad-pool_sec-root_describe></package_upanishad-pool_sec-root_describe>', '', '', function(opts) {
});

riot.tag2('package_upanishad-pool_sec-root_describe', '<section-container title="概要"> <section-container title="スナップショットとレストア(トランなし)図"> <p><pre style="font-size:12px; line-height:12px;">\n +--------------------+                                                    +--------------------+\n | POOL               |                                                    | POOL               |\n |  +--------------+  |                                                    |  +--------------+  |\n |  | MEMES        |  |   snapshot                              restore    |  | MEMES        |  |\n |  | +----------+ |  |     |                                       |      |  | +----------+ |  |\n |  | | MEME     | |  |     v                                       v      |  | | MEME     | |  |\n |  | | [list] --------------------> [snapshot file (meme)] --------------------->[list]   | |  |\n |  | +----------+ |  |     :                                       :      |  | +----------+ |  |\n |  +--------------+  |     :                                       :      |  +--------------+  |\n |                    |     :                                       :      |                    |\n |  +--------------+  |     :                                       :      |  +--------------+  |\n |  | ID-COUNTER   |  |     :                                       :      |  | ID-COUNTER   |  |\n |  | [???] -----------------------> [snapshot file (id)] --------------------->[???]        |  |\n |  +--------------+  |     :                                       :      |  +--------------+  |\n |                    |     :                                       :      |                    |\n |  +--------------+  |     :                                       :      |  +--------------+  |\n |  | OPTIONS      |  |     :                                       :      |  | OPTIONS      |  |\n |  | [ht] ------------------------> [snapshot file [options)] ---------------->[ht]         |  |\n |  +--------------+  |     ^                                       ^      |  +--------------+  |\n |                    |     |                                       |      |                    |\n +--------------------+                                                    +--------------------+\n            </pre></p> </section-container> </section-container> <section-container title="永続化/トランザクションのフロー"> <p><pre style="font-size:12px; line-height:12px;">........</pre></p> </section-container> </section-container> <section-container title="CLASSES"> <section-container title="CLASS図"> <section-contents title="CLASS図：未来"> <p><pre style="font-size:12px; line-height:12px;">\n +-------------+      +----------------+      +---------+      +-----------------+      +----------------+\n | pool-core   |      | pool-option    |      | pool-id |      | pool-preference |      | horny          |\n |=============|      |================|      |=========|      |=================|      |================|\n | memes       | ht   | options        | ht   | %id     | ht   | preference      | ht   | root-objects   | ht\n | indexes     | ht   +----------------+      +---------+      +-----------------+      | index-objects  |\n +-------------+           |                       |                |                   +----------------+\n       |                   |                       |                |                        |\n       |                   |                       |                |                        |\n       +-------------------+-----------------------+----------------+------------------------+\n       |\n       |            +-------------------------------------+            +------------------------------+\n       |            | secretary                           |            | librarian                    |\n       |            |=====================================|            |==============================|\n       |            | transaction-log-file-extension      | string     | snapshot-file-extension      | string\n       |            | transaction-log-directory           | pathname   | snapshot-directory           | pathname\n       |            | transaction-log-directory-backup    | pathname   | snapshot-directory-backup    | pathname\n       |            | transaction-log                     | pathname   | snapshot                     | pathname\n       |            | transaction-log-stream              | stream     | snapshot-serializer          |\n       |            | transaction-hook                    | function   | snapshot-serialization-state |\n       |            | transaction-log-serializer          |            | snapshot-deserializer        |\n       |            | transaction-log-serialization-state |            +------------------------------+\n       |            | transaction-log-deserializer        |                          |\n       |            +-------------------------------------+                          |\n       |                         |                                                   |\n       |                         |                                                   |\n       +-------------------------+---------------------------------------------------+\n       |\n       |\n       V\n+--------------------+          +-------------+\n| pool               |          | transaction |\n|====================|          |=============|\n|--------------------|          | args        |\n| snapshot           |          | function    |\n| restore            |          +-------------+\n| backup             |\n| execut-transaction |\n+--------------------+\n       |\n       |\n       V\n+--------------+\n| guarded-pool |\n|==============|\n| guard        |\n+--------------+\n                </pre></p> </section-contents> </section-container> <section-contents title="CLASS図：古い"> <p><pre style="font-size:12px; line-height:12px;">\n  +-----------+     +------------------------+            +---------------------+\n  | pool-core |     | pool-transaction       |            | pool-persistence    |\n  |===========|     |========================|            |=====================|\n  | memes     | ht  | transaction-hook       | function   | serializer          | function\n  | indexes   | ht  | snapshot               | ht         | deserializer        | function\n  | options   | ht  | transaction-log        | pathname   | serialization-state | ???\n  +-----------+     | transaction-log-stream | stream     +---------------------+\n       |            +------------------------+                        |\n       |                         |                                    |\n       |                         |                                    |\n       +-------------------------+------------------------------------+\n       |\n       |\n       V\n+----------------+              +-------------+\n| pool           |              | transaction |\n|================|              |=============|\n| directory      |              | args        |\n| file-extension |              | function    |\n+----------------+              +-------------+\n       |\n       |\n       V\n+--------------+\n| guarded-pool |\n|==============|\n| guard        |\n+--------------+\n            </pre></p> </section-contents> </section-container>', '', '', function(opts) {
});

riot.tag2('package_upanishad-pool_sec-root_operators', '<section class="section"> <div class="container"> <h1 class="title">Operators</h1> <h2 class="subtitle"></h2> <section class="section"> <div class="container"> <div class="contents"> <operators data="{operators.pool}"></operators> </div> </div> </section> <section class="section"> <div class="container"> <div class="contents"> <operators data="{operators.meme}"></operators> </div> </div> </section> <section class="section"> <div class="container"> <div class="contents"> <operators data="{operators.index}"></operators> </div> </div> </section> </div> </section>', '', '', function(opts) {
     this.operators = {
         pool: [
             {type: 'Function',         code: 'make-pool',       description: 'プールを作成します。'},
             {type: 'Generic Function', code: 'start',           description: 'トランザクションの記録を開始する。'},
             {type: 'Generic Function', code: 'stop',            description: 'トランザクションログの記録を停止します。'},
             {type: 'Generic Function', code: 'snapshot',        description: 'スナップショットを作成する。'},
             {type: 'Generic Function', code: 'restore',         description: 'スナップショットとトランザクションログから meme を戻す。'},
             {type: 'Generic Function', code: 'totally-destroy', description: 'いろいろ全部削除します。'},
         ],
         meme: [
             {type: 'Generic Function', code: 'tx-create-meme',  description: ''},
             {type: 'Generic Function', code: 'tx-update-meme',  description: ''},
             {type: 'Generic Function', code: 'tx-delete-meme',  description: ''},
             {type: 'Generic Function', code: 'find-meme',       description: ''},
             {type: 'Generic Function', code: 'get-meme',        description: ''},
         ],
         index: [
             {type: 'Generic Function', code: 'get-index',       description: 'upanishad.pool.core'},
             {type: 'Generic Function', code: 'tx-add-index',    description: 'upanishad.pool.core'},
             {type: 'Generic Function', code: 'tx-remove-index', description: 'upanishad.pool.core'},
         ]

     };

});

riot.tag2('defgeneric-function_package_name', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title">Genric Function <b>start</b></h1> <h2 class="subtitle"></h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Syntax</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', '', function(opts) {
});

riot.tag2('defgeneric-function_pool_find-meme', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>FIND-MEME</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="FIND-MEME" args="pool class" keys="slot value test" results="list"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_get-meme', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>GET-MEME</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="GET-MEME" args="pool class" keys="%id slot value" results="meme"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_restore', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>SNAPSHOT</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="RESTORE" args="pool" results="pool"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p>スナップショットとトランザクションログから meme を戻す。</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_snapshot', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>SNAPSHOT</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="SNAPSHOT" args="pool" results="pool"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p>スナップショットを作成する。</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_start', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>START</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="START" args="pool" results="pool"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p>トランザクションの記録を開始する。</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_stop', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>STOP</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="STOP" args="pool" keys="abort" results="pool"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p>トランザクションログの記録を停止します。</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_totally-destroy', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>TOTALLY-DESTROY</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="TOTALLY-DESTROY" args="pool" keys="abort" results="pool"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p>いろいろ全部削除します。</p> <ul> <li>登録しているものを全部削除する。</li> <li>トランザクションログを全部削除する。</li> <li>スナップショットを全部削除する。</li> </ul> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_tx-create-meme', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>TX-CREATE-MEME</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="TX-CREATE-MEME" args="pool class" options="slots-and-values" results="meme"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_tx-delete-meme', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>TX-DELETE-MEME</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="TX-DELETE-MEME" args="pool class %id" results="pool"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool_tx-update-meme', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>TX-UPDATE-MEME</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="TX-UPDATE-MEME" args="pool class" optional="slots-and-values" results="meme"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('upanishad-pool', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('defgeneric-function_pool-core_get-index', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>GET-INDEX</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="GET-INDEX" args="pool class slot" results="index"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool-core_tx-add-index', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>TX-ADD-INDEX</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="TX-ADD-INDEX" args="pool index" results="index"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('defgeneric-function_pool-core_tx-remove-index', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1"><b>TX-REMOVE-INDEX</b></h1> <h2 class="subtitle">Genric Function</h2> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Syntax</h1> <h2 class="subtitle"></h2> <div class="contents"> <symbol-syntax name="TX-REMOVE-INDEX" args="pool index" results="pool"></symbol-syntax> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Method Signatures</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Arguments and Values</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Examples</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Affected By</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Exceptional Situations</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">See Also</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-4">Notes</h1> <h2 class="subtitle"></h2> <div class="contents">None.</div> </div> </section>', '', 'class="symbol"', function(opts) {
});

riot.tag2('package_upanishad-pool-core_sec-root', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title is-1">UPANISHAD.POOL.CORE</h1> <h2 class="subtitle">Package</h2> </div> </div> </section> <section-container title="概要"> </section-container> <section-container title="CLASSES"> <section-contents title="CLASS図"> <p> <pre style="font-size:12px; line-height:12px;">\n                </pre> </p> </section-contents> </section-container> <package_upanishad-pool-core_sec-root_operators> </package_upanishad-pool-core_sec-root_operators>', '', '', function(opts) {
});

riot.tag2('package_upanishad-pool-core_sec-root_operators', '<section class="section"> <div class="container"> <h1 class="title">Operators</h1> <h2 class="subtitle"></h2> <div class="contents"> <operators data="{operators}"></operators> </div> </div> </section>', '', '', function(opts) {
     this.operators = [
         {type: 'Generic Function', code: 'get-index',       description: ''},
         {type: 'Generic Function', code: 'tx-add-index',    description: ''},
         {type: 'Generic Function', code: 'tx-remove-index', description: ''},
         {type: 'Generic Function', code: 'clear-indexes',   description: ''},
         {type: 'Generic Function', code: 'get-memes',       description: ''},
         {type: 'Generic Function', code: 'ensure-memes',    description: ''},
         {type: 'Generic Function', code: 'clear-memes',     description: ''},
     ];
});

riot.tag2('upanishad-pool-core', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('package_upanishad-pool-librarian_sec-classes', '<section class="section"> <div class="container"> <section-container title="CLASSES"> <section-contents title="CLASS図"> <p> <pre style="font-size:12px; line-height:12px;">\n                        </pre> </p> </section-contents> </section-container> </div> </section>', '', '', function(opts) {
});

riot.tag2('package_upanishad-pool-librarian_sec-operators', '<section class="section"> <div class="container"> </div> </section>', '', '', function(opts) {
});

riot.tag2('package_upanishad-pool-librarian_sec-others', '<section class="section"> <div class="container"> </div> </section>', '', '', function(opts) {
});

riot.tag2('package_upanishad-pool-librarian_sec-readme', '<section class="section"> <div class="container"> <h1 class="title">概要</h1> <div> <p>オブジェクトを永続化するライブラリです。</p> </div> </div> </section>', '', '', function(opts) {
});

riot.tag2('package_upanishad-pool-librarian_sec-root', '<section-header title="Package: UPANISHAD.POOL.LIBRARIAN"></section-header> <section class="section"> <div class="container"> <div class="tabs"> <ul> <li class="is-active"><a>README</a></li> <li><a>Classes</a></li> <li><a>Operators</a></li> <li><a>Others</a></li> </ul> </div> </div> </section> <div> <package_upanishad-pool-librarian_sec-readme></package_upanishad-pool-librarian_sec-readme> <package_upanishad-pool-librarian_sec-classes></package_upanishad-pool-librarian_sec-classes> <package_upanishad-pool-librarian_sec-operators></package_upanishad-pool-librarian_sec-operators> <package_upanishad-pool-librarian_sec-others></package_upanishad-pool-librarian_sec-others> </div>', '', '', function(opts) {
});

riot.tag2('upanishad-pool-librarian', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('package_upanishad-pool-perpetuation_sec-root', '<section-header title="Package: UPANISHAD.POOL.PERPETUATION"></section-header> <section-container title="概要"> <div class="contents"> <p>永続化機能のパッケージです。</p> </div> </section-container> <section-container title="CLASSES"> </section-container>', '', '', function(opts) {
});

riot.tag2('upanishad-pool-perpetuation', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});

riot.tag2('package_upanishad-pool-secretary_sec-root', '<section-header title="Package: UPANISHAD.POOL.SECRETARY"></section-header> <section-container title="概要"> <div class="contents"> <p>トランザクション機能のパッケージです。</p> <p>実行した処理を記録していくので、書記(secretary)が名前の由来です。</p> </div> </section-container> <section-container title="CLASSES"> <section-container title="Class: SECRETARY"> </section-container> <section-container title="Class: TRANSACTION"> </section-container> </section-container> <section-container title="Conditions"> <section-container title="Condition: NO-ROLLBACK-ERROR"> </section-container> </section-container> <section-container title="Operators"> <section-container title="Genric function: INITIATES-ROLLBACK"> </section-container> <section-container title="Genric function: LOG-TRANSACTION"> </section-container> <section-container title="Genric function: LOG-TRANSACTION"> </section-container> <section-container title="Genric function: EXECUTE-ON"> </section-container> </section-container>', '', '', function(opts) {
});

riot.tag2('upanishad-pool-secretary', '', '', '', function(opts) {
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
});


riot.tag2('page-tabs2', '<section class="section" style="padding-top: 0.5rem; padding-bottom: 0.5rem;"> <div class="container"> <div class="tabs"> <ul> <li each="{opts.tabs}" class="{opts.active_tab==code ? \'is-active\' : \'\'}"> <a code="{code}" onclick="{opts.clickTab}">{label}</a> </li> </ul> </div> </div> </section>', '', '', function(opts) {
});

riot.tag2('persistence-sec_classes', '<section class="section"> <div class="container"> <h1 class="title">Class: serialization-state</h1> <h2 class="subtitle"></h2> <div class="contents"> <table class="table"> <thead> <tr> <th>name</th> <th>accessor</th> <th>reader</th> <th>initform</th> </tr> </thead> <tbody> <tr each="{class.slots}"> <td>{name}</td> <td>{accessor}</td> <td>{reader}</td> <td>{initform}</td> </tr> </tbody> </table> </div> </div> </section>', '', '', function(opts) {
     this.class = {
         slots: [
             { name: 'counter',     accessor: 'get-counter',                 initform: '0' },
             { name: 'hashtable',   accessor: null, reader: 'get-hashtable', initform: '(make-hash-table :test \'eq :size 1024 :rehash-size 2.0)' },
             { name: 'known-slots', accessor: null,                          initform: '(make-hash-table)' },
         ]
     }
});

riot.tag2('persistence-sec_operators', '<section class="section"> <div class="container"> <div class="contents"> <table class="table"> <thead> <tr> <th>Type</th> <th>Symbol</th> <th>Description</th></tr> </thead> <tbody> <tr each="{operators}"> <td>{type}</td> <td> <a href="#persistence/{code}">{code}</a> </td> <td>{description}</td> </tr> </tbody> </table> </div> </div> </section>', '', '', function(opts) {
     this.operators = [
         { code: 'serialize-xml-internal',             type: 'Generic Function', description: '' },
         { code: 'deserialize-xml-new-element',        type: 'Function',         description: '' },
         { code: 'deserialize-xml-new-element-aux',    type: 'Generic Function', description: '' },
         { code: 'deserialize-xml-finish-element',     type: 'Function',         description: '' },
         { code: 'deserialize-xml-finish-element-aux', type: 'Generic Function', description: '' },
         { code: 'reset',                              type: 'Generic Function', description: '' },
         { code: 'reset-known-slots',                  type: 'Generic Function', description: '' },
         { code: 'known-object-id',                    type: 'Generic Function', description: '' },
         { code: 'set-known-object',                   type: 'Generic Function', description: '' },
         { code: 'serializable-slots',                 type: 'Generic Function', description: '' },
         { code: 'get-serializable-slots',             type: 'Generic Function', description: '' },
         { code: 'get-serializable-slots',             type: 'Generic Function', description: '' },
         { code: 'sequence-type-and-length',           type: 'Function',         description: '' },
         { code: 'serialize-sexp-internal',            type: 'Generic Function', description: '' },
     ];
});

riot.tag2('persistence-sec_readme', '<section class="section"> <div class="container"> <h1 class="title">Description</h1> <h2 class="subtitle"></h2> <div class="contents"> <p>meme のみ一意性が担保される。それ以外のオブジェクトは別物として扱われる。</p> <p>ハッシュテーブル、シーケンス、構造体もそう。</p> <p></p> </div> </div> </section>', '', '', function(opts) {
});

riot.tag2('persistence-sec_root', '<section-header title="永続化"></section-header> <page-tabs2 tabs="{tabs}" active_tab="{active_tab}" click-tab="{clickTab}"></page-tabs2> <div> <persistence-sec_readme class="hide"></persistence-sec_readme> <persistence-sec_classes class="hide"></persistence-sec_classes> <persistence-sec_operators class="hide"></persistence-sec_operators> <persistence-sec_xml class="hide"></persistence-sec_xml> </div>', '', '', function(opts) {
     this.tabs = [
         { code: 'readme',    label: 'Readme',    tag: 'persistence-sec_readme' },
         { code: 'classes',   label: 'Classes',   tag: 'persistence-sec_classes' },
         { code: 'operators', label: 'Operators', tag: 'persistence-sec_operators' },
         { code: 'xml',       label: 'Xml', tag: 'persistence-sec_xml' },
     ];
     this.active_tab = null;
     this.clickTab = (e) => {
         let code = e.target.getAttribute('code');

         this.switchTab(code);

         this.update();
     };
     this.switchTab = (code) => {
         if (this.active_tab==code)
             return;

         let active = null;
         for (var i in this.tabs) {
             let tab = this.tabs[i];
             let tag = this.tags[tab.tag];

             tag.root.classList.add('hide');

             if (tab.code==code)
                 active = tag;
         }

         if (active)
             active.root.classList.remove('hide');

         this.active_tab = code;
     };
     this.on('mount', () => {
         this.switchTab(this.tabs[0].code);
     });
});

riot.tag2('persistence-sec_xml', '<section class="section"> <div class="container"> <h1 class="title">Description</h1> <div class="contents"> <table class="table"> <thead> <tr> <th>Object</th> <th>XML Tag</th> <th>Description</th> </tr> </thead> <tbody> <tr each="{method_signatures}"> <td>{object}</td> <td>{tag}</td> <td>{description}</td> </tr> </tbody> </table> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Serialize</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Description</h1> <div class="contents"> </div> </div> </section>', '', '', function(opts) {
     this.method_signatures = [
         { object: 'null',             tag: '<NULL/>',                                   description: '' },
         { object: 't',                tag: '<SYMBOL></SYMBOL>',                         description: '' },
         { object: 'symbol',           tag: '<TRUE/>',                                   description: '' },
         { object: 'string',           tag: '<STRING></STRING>',                         description: '' },
         { object: 'character',        tag: '<CHARACTER></CHARACTER>',                   description: '' },
         { object: 'complex',          tag: '<COMPLEX></COMPLEX>',                       description: '' },
         { object: 'float',            tag: '<FLOAT></FLOAT>',                           description: '' },
         { object: 'integer',          tag: '<INT></INT>',                               description: '' },
         { object: 'ratio',            tag: '<RATIO></RATIO>',                           description: '' },
         { object: 'hash-table',       tag: '<HASH-TABLE TEST="" SIZE=""></HASH-TABLE>', description: 'IDは削除しました。' },
         { object: 'sequence',         tag: '<SEQUENCE CLASS="" SIZE=""></SEQUENCE>',    description: 'IDは削除しました。' },
         { object: 'standard-object',  tag: '<OBJECT CLASS=""></OBJECT>',                description: 'IDは削除しました。' },
         { object: 'structure-object', tag: '<STRUCT CLASS=""></STRUCT>',                description: 'IDは削除しました。' },
         { object: 'slot-index',       tag: '<INDEX CLASS=""></INDEX>',                  description: '' },
         { object: 'memes',            tag: '<MEMES CLASS=""></MEMES>',                  description: '' },
         { object: 'meme',             tag: '<MEME ID="" CLASS=""></MEME>',              description: '' },
     ]
});

riot.tag2('upanishad_persistence_deserialize-xml-finish-element-aux', '<section-header title="Generic Function: DESERIALIZE-XML-NEW-ELEMENT"></section-header> <section class="section"> <div class="container"> <h1 class="title is-3">Syntax:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Method Signatures:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Arguments and Values:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Description:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Examples:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Affected By:</h1> <div class="contents"> <p>None.</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Exceptional Situations:</h1> <div class="contents"> <p>None.</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">See Also:</h1> <div class="contents"> <p>None.</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Notes:</h1> <div class="contents"> <p>None.</p> </div> </div> </section>', '', '', function(opts) {
});

riot.tag2('upanishad_persistence_deserialize-xml-finish-element', '<section-header title="Function: DESERIALIZE-XML-FINISH-ELEMENT"></section-header>', '', '', function(opts) {
});

riot.tag2('upanishad_persistence_deserialize-xml-new-element-aux', '<section-header title="Generic Function: DESERIALIZE-XML-NEW-ELEMENT-AUX"></section-header> <section class="section"> <div class="container"> <h1 class="title is-3">Syntax:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Method Signatures:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Arguments and Values:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Description:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Examples:</h1> <div class="contents"> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Affected By:</h1> <div class="contents"> <p>None.</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Exceptional Situations:</h1> <div class="contents"> <p>None.</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">See Also:</h1> <div class="contents"> <p>None.</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-3">Notes:</h1> <div class="contents"> <p>None.</p> </div> </div> </section>', '', '', function(opts) {
});

riot.tag2('upanishad_persistence_deserialize-xml-new-element', '<section-header title="Function: DESERIALIZE-XML-NEW-ELEMENT"></section-header>', '', '', function(opts) {
});

riot.tag2('upanishad_persistence_serialize-xml-internal', '<section-header title="Generic Function: SERIALIZE-SEXP-INTERNAL"></section-header> <section class="section"> <div class="container"> <h1 class="title">Syntax:</h1> <div class="contents"> <p>serialize-xml-internal object stream serialization-state ⇒ ???</p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Method Signatures:</h1> <div class="contents"> <table class="table"> <thead> <tr> <th>object</th> <th>stream</th> <th>serializatio_state</th> </tr> </thead> <tbody> <tr each="{method_signatures}"> <td>{object}</td> <td>{stream}</td> <td>{serializatio_state}</td> </tr> </tbody> </table> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Arguments and Values:</h1> <h2 class="subtitle"></h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Description:</h1> <div class="contents"> <table class="table"> <thead> <tr> <th>Object</th> <th>XML Tag</th> </tr> </thead> <tbody> <tr each="{method_signatures}"> <td>{object}</td> <td>{tag}</td> </tr> </tbody> </table> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Examples:</h1> <div class="contents"><p>None.</p></div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Affected By:</h1> <div class="contents"><p>None.</p></div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">Exceptional Situations:</h1> <div class="contents"><p>None.</p></div> </div> </section> <section class="section"> <div class="container"> <h1 class="title">See Also:</h1> <div class="contents"><p>None.</p></div> </div> </section>', '', '', function(opts) {
     this.method_signatures = [
         { object: 'null',             stream: null, serializatio_state: null, tag: '<NULL/>' },
         { object: 't',                stream: null, serializatio_state: null, tag: '<SYMBOL></SYMBOL>' },
         { object: 'symbol',           stream: null, serializatio_state: null, tag: '<TRUE/>' },
         { object: 'string',           stream: null, serializatio_state: null, tag: '<STRING></STRING>' },
         { object: 'character',        stream: null, serializatio_state: null, tag: '<CHARACTER></CHARACTER>' },
         { object: 'complex',          stream: null, serializatio_state: null, tag: '<COMPLEX></COMPLEX>' },
         { object: 'float',            stream: null, serializatio_state: null, tag: '<FLOAT></FLOAT>' },
         { object: 'integer',          stream: null, serializatio_state: null, tag: '<INT></INT>' },
         { object: 'ratio',            stream: null, serializatio_state: null, tag: '<RATIO></RATIO>' },
         { object: 'hash-table',       stream: null, serializatio_state: null, tag: '<HASH-TABLE ID="" TEST="" SIZE=""></HASH-TABLE>' },
         { object: 'sequence',         stream: null, serializatio_state: null, tag: '<SEQUENCE ID="" CLASS="" SIZE=""></SEQUENCE>' },
         { object: 'standard-object',  stream: null, serializatio_state: null, tag: '<OBJECT ID="" CLASS=""></OBJECT>' },
         { object: 'structure-object', stream: null, serializatio_state: null, tag: '<STRUCT ID="" CLASS=""></STRUCT>' },
         { object: 'slot-index',       stream: null, serializatio_state: null, tag: '<INDEX CLASS=""></INDEX>' },
         { object: 'memes',            stream: null, serializatio_state: null, tag: '<MEMES CLASS=""></MEMES>' },
         { object: 'meme',             stream: null, serializatio_state: null, tag: '<MEME ID="" CLASS=""></MEME>' },
     ]
});

riot.tag2('package_upanishad_sec-root', '<section-header title="Package: UPANISHAD"></section-header> <section-container title="概要"> </section-container> <section-container title="CLASSES"> </section-container> <package_upanishad_sec-root_operators></package_upanishad_sec-root_operators>', '', '', function(opts) {
});

riot.tag2('package_upanishad_sec-root_operators', '<section class="section"> <div class="container"> <h1 class="title">Operators</h1> <h2 class="subtitle"> </h2> <div class="contents"> <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth"> <thead> <tr> <th>Type</th> <th>Symbol</th> <th>Description</th> <th>From Package</th> </tr> </thead> <tbody> <tr each="{operators}"> <td>{type}</td> <td><b>{symbol}</b></td> <td>{description}</td> <td>{from_package}</td> </tr> </tbody> </table> </div> </div> </section>', '', '', function(opts) {
     this.operators = [
         {type:'Accessr',          symbol:'%id',            description:'', from_package: 'upanishad.meme'},
         {type:'Class',            symbol:'meme',           description:'', from_package: 'upanishad.meme'},
         {type:'Function',         symbol:'make-pool',      description:'Pool 作成', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'start',          description:'Pool トランザクションログ 記録開始 @未実装', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'stop',           description:'Pool トランザクションログ 記録停止 ', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'tx-create-meme', description:'meme 作成(C)', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'tx-delete-meme', description:'meme 削除(D)', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'find-meme',      description:'meme 検索(R)', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'get-meme',       description:'meme 取得(R)', from_package: 'upanishad.pool'}
     ];
});

riot.tag2('usage-sec_root', '<section-header title="Usage"></section-header> <usage-sec_root_pool-basic></usage-sec_root_pool-basic> <usage-sec_root_meme-basic></usage-sec_root_meme-basic> <usage-sec_root_index-basic></usage-sec_root_index-basic> <usage-sec_root_persistence-basic></usage-sec_root_persistence-basic>', 'usage-sec_root .usage-section-root > .section .section,[data-is="usage-sec_root"] .usage-section-root > .section .section{ margin-left: 66px; padding-bottom: 0px; padding-top: 33px; } usage-sec_root .usage-section-root .container > .content,[data-is="usage-sec_root"] .usage-section-root .container > .content{ padding-left: 22px; }', '', function(opts) {
});

riot.tag2('usage-sec_root_index-basic', '<section class="section"> <div class="container"> <h1 class="title is-4">index の追加/削除</h1> <h2 class="subtitle"> </h2> </div> <section class="section"> <div class="container"> <h1 class="title is-5">スロットインデックスの追加</h1> <h2 class="subtitle">tx-add-index</h2> <div class="content"> <p><pre>\n                    </pre></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-5">スロットインデックスの取得</h1> <h2 class="subtitle">get-index</h2> <div class="content"> <p><pre>\n                    </pre></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-5">スロットインデックスの削除</h1> <h2 class="subtitle">tx-remove-index</h2> <div class="content"> <p><pre>\n                    </pre></p> </div> </div> </section> </section>', '', 'class="usage-section-root"', function(opts) {
});

riot.tag2('usage-sec_root_meme-basic', '<section class="section"> <div class="container"> <h1 class="title is-4">meme の追加/変更/削除</h1> <h2 class="subtitle"> </h2> </div> <section class="section"> <div class="container"> <h1 class="title is-5">Meme の追加</h1> <h2 class="subtitle"> </h2> <div class="content"> <p><pre>\n(defclass user (meme)\n  ((code :accessor code :initarg :code :initform nil)\n   (name :accessor name :initarg :name :initform "")))\n                    </pre></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-5">Meme の取得</h1> <h2 class="subtitle">TX-CREATE-MEME を利用します。</h2> <div class="content"> <p><pre>\n(tx-create-meme *pool* \'user\n                \'((code . :user01)\n                  (name . "test-user-01")))\n(tx-create-meme *pool* \'user\n                \'((code . :user02)\n                  (name . "test-user-01")))\n                    </pre></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-5">Meme の検索</h1> <h2 class="subtitle"> </h2> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-5">Meme の削除</h1> <h2 class="subtitle">GET-MEME を利用します。</h2> <div class="content"> <p><pre>\n(get-meme *pool* \'user :%id 1)\n(get-meme *pool* \'user :%id 2)\n                    </pre></p> </div> </div> </section> </section>', '', 'class="usage-section-root"', function(opts) {
});

riot.tag2('usage-sec_root_persistence-basic', '<section class="section"> <div class="container"> <h1 class="title is-4">永続化</h1> <h2 class="subtitle"> </h2> </div> <section class="section"> <div class="container"> <h1 class="title is-5">スナップショット</h1> <h2 class="subtitle"> </h2> <div class="content"> <p><pre>(snapshot *pool*)</pre></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-5">レストア</h1> <h2 class="subtitle"> </h2> <div class="content"> <p><pre>(restore *pool*)</pre></p> </div> </div> </section> </section>', '', 'class="usage-section-root"', function(opts) {
});

riot.tag2('usage-sec_root_pool-basic', '<section class="section"> <div class="container"> <h1 class="title is-4">POOL</h1> <h2 class="subtitle"></h2> </div> <section class="section"> <div class="container"> <h1 class="title is-5">作成</h1> <h2 class="subtitle"></h2> <div class="content"> <p><pre>\n(in-package :upanishad)\n(make-pool #P"~/tmp/")\n                    </pre></p> </div> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-5">トランザクションログの収集開始</h1> <h2 class="subtitle"></h2> <article class="content"> <p>特定のオペレーションはなく make-pool したタイミングからトランザクションログの記録が開始します。</p> <p>しかし、stop オペレータがあるので start があっても良さそう。</p> </article> </div> </section> <section class="section"> <div class="container"> <h1 class="title is-5">トランザクションログの収集停止</h1> <h2 class="subtitle"></h2> <article class="content"> <p>この操作でトランザクションログの記録が停止されます</p> <p><pre>(stop (upanishad:make-pool #P"~/tmp/"))</pre></p> </article> </div> </section> </section>', '', 'class="usage-section-root"', function(opts) {
});

riot.tag2('variable-100', '<variable-header></variable-header>', '', '', function(opts) {
});

riot.tag2('variable-header', '<section class="hero"> <div class="hero-body"> <div class="container"> <h1 class="title">Variable: source.name</h1> <h2 class="subtitle">YYY</h2> </div> </div> </section>', '', '', function(opts) {
     this.dataID = () => {
         return location.hash.split('/').reverse()[0] * 1;
     };
     this.loadSource = () => {
         return STORE.get('data.nodes.ht')[this.dataID()];
     }

     this.source = null;
     this.on('before-mount', () => {
         this.source = this.loadSource();
     });
     this.on('update', () => {
         this.source = this.loadSource();
     });
});
