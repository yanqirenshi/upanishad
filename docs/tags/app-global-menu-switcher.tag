<app-global-menu-switcher>

    <div>
        <div>
            <p></p>
        </div>

        <div>
            <p> < </p>
        </div>
    </div>

    <style>
     app-global-menu-switcher > div {
         display: flex;
         color: #BFA742;
         border-top: 1px solid #BFA742;
     }
     app-global-menu-switcher > div > div {
         padding: 8px;
     }
     app-global-menu-switcher > div > div:first-child {
         flex-grow: 1;
     }
     app-global-menu-switcher > div > div:last-child {
         border-left: 1px solid #BFA742;
         font-weight: bold;
     }
     app-global-menu-switcher > div > div:last-child:hover {
         color: #830411;
         background: #BFA742;
     }
    </style>

</app-global-menu-switcher>
