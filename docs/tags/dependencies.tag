<dependencies>

    <section class="section">
        <div class="container">
            <h1 class="title">Dependencies</h1>
            <h2 class="subtitle"></h2>

            <div class="contents">
                <table class="table is-bordered is-striped is-narrow is-hoverable">
                    <thead>
                        <tr>
                            <th rowspan="2">ID</th>
                            <th colspan="3">From</th>
                            <th rowspan="2">Type</th>
                            <th colspan="3">From</th>
                        </tr>

                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Class</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Class</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr each={obj in list()}>
                            <td>{obj.id}</td>
                            <td>{obj.from.id}</td>
                            <td>{obj.from.name}</td>
                            <td>{obj.from._class}</td>
                            <td>{obj.type}</td>
                            <td>{obj.to.id}</td>
                            <td>{obj.to.name}</td>
                            <td>{obj.to._class}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <script>
     this.dataID = () => {
         return location.hash.split('/').reverse()[0]*1;
     };
     this.list = () => {
         let index = STORE.get('data.indexes.edge')
         let node_id = this.dataID();

         let edges = {};

         this.mergeEdges = (index) => {
             if (!index || !index.edges)
                 return;

             let ht = index.edges;
             for (let key in ht)
                 edges[key] = ht[key];
         };

         this.mergeEdges(index.from[node_id]);
         this.mergeEdges(index.to[node_id]);

         let list = [];
         for (let key in edges)
             list.push(edges[key]);

         return list;
     }
    </script>

</dependencies>
