<variable-header>

    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">Variable: source.name</h1>
                <h2 class="subtitle">YYY</h2>
            </div>
        </div>
    </section>


    <script>
     this.dataID = () => {
         return location.hash.split('/').reverse()[0] * 1;
     };
     this.loadSource = () => {
         return STORE.get('data.nodes.ht')[this.dataID()];
     }
    </script>

    <script>
     this.source = null;
     this.on('before-mount', () => {
         this.source = this.loadSource();
     });
     this.on('update', () => {
         this.source = this.loadSource();
     });
    </script>

</variable-header>
