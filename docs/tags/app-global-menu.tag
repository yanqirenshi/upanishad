<app-global-menu>

    <div ref="root">
        <app-global-menu-bland></app-global-menu-bland>

        <app-global-menu-item each={obj in list()}
                              source={obj}
                              active_page={activePage()}></app-global-menu-item>

        <div style="flex-grow:1;"></div>

        <app-global-menu-switcher></app-global-menu-switcher>
    </div>

    <script>
     this.on('update', () => {
         let w = this.refs.root.clientWidth;

         this.root.style.width = w + 'px'
     });
     this.on('mount', () => {
         let w = this.refs.root.clientWidth;

         this.root.style.width = w + 'px'
     });
    </script>

    <script>
     this.list = () => {
         return STORE.get('site.pages');
     };
     this.activePage = () => {
         return STORE.get('site.active_page');
     };
    </script>

    <style>
     app-global-menu {
         display: block;
     }
     app-global-menu > div {
         position: fixed;
         height: 100vh;

         display: flex;
         flex-direction: column;

         background: #830411;
     }
    </style>

</app-global-menu>
