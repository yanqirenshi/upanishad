<package_upanishad-pool-secretary_sec-root>
    <section-header title="Package: UPANISHAD.POOL.SECRETARY"></section-header>

    <section-container title="概要">
        <div class="contents">
            <p>トランザクション機能のパッケージです。</p>
            <p>実行した処理を記録していくので、書記(secretary)が名前の由来です。</p>
        </div>
    </section-container>

    <section-container title="CLASSES">
        <section-container title="Class: SECRETARY">
        </section-container>
        <section-container title="Class: TRANSACTION">
        </section-container>
    </section-container>

    <section-container title="Conditions">
        <section-container title="Condition: NO-ROLLBACK-ERROR">
        </section-container>
    </section-container>

    <section-container title="Operators">
        <section-container title="Genric function: INITIATES-ROLLBACK">
        </section-container>
        <section-container title="Genric function: LOG-TRANSACTION">
        </section-container>
        <section-container title="Genric function: LOG-TRANSACTION">
        </section-container>
        <section-container title="Genric function: EXECUTE-ON">
        </section-container>
    </section-container>

</package_upanishad-pool-secretary_sec-root>
