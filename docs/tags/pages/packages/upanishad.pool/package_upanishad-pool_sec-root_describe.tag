<package_upanishad-pool_sec-root_describe>
    <section-container title="概要">
        <section-container title="スナップショットとレストア(トランなし)図">
            <p><pre style="font-size:12px; line-height:12px;">
 +--------------------+                                                    +--------------------+
 | POOL               |                                                    | POOL               |
 |  +--------------+  |                                                    |  +--------------+  |
 |  | MEMES        |  |   snapshot                              restore    |  | MEMES        |  |
 |  | +----------+ |  |     |                                       |      |  | +----------+ |  |
 |  | | MEME     | |  |     v                                       v      |  | | MEME     | |  |
 |  | | [list] --------------------> [snapshot file (meme)] --------------------->[list]   | |  |
 |  | +----------+ |  |     :                                       :      |  | +----------+ |  |
 |  +--------------+  |     :                                       :      |  +--------------+  |
 |                    |     :                                       :      |                    |
 |  +--------------+  |     :                                       :      |  +--------------+  |
 |  | ID-COUNTER   |  |     :                                       :      |  | ID-COUNTER   |  |
 |  | [???] -----------------------> [snapshot file (id)] --------------------->[???]        |  |
 |  +--------------+  |     :                                       :      |  +--------------+  |
 |                    |     :                                       :      |                    |
 |  +--------------+  |     :                                       :      |  +--------------+  |
 |  | OPTIONS      |  |     :                                       :      |  | OPTIONS      |  |
 |  | [ht] ------------------------> [snapshot file [options)] ---------------->[ht]         |  |
 |  +--------------+  |     ^                                       ^      |  +--------------+  |
 |                    |     |                                       |      |                    |
 +--------------------+                                                    +--------------------+
            </pre></p>
        </section-container>
    </section-container>

    <section-container title="永続化/トランザクションのフロー">
        <p><pre style="font-size:12px; line-height:12px;">........</pre></p>
    </section-container>
    </section-container>

    <section-container title="CLASSES">
        <section-container title="CLASS図">
            <section-contents title="CLASS図：未来">
                <p><pre style="font-size:12px; line-height:12px;">
 +-------------+      +----------------+      +---------+      +-----------------+      +----------------+
 | pool-core   |      | pool-option    |      | pool-id |      | pool-preference |      | horny          |
 |=============|      |================|      |=========|      |=================|      |================|
 | memes       | ht   | options        | ht   | %id     | ht   | preference      | ht   | root-objects   | ht
 | indexes     | ht   +----------------+      +---------+      +-----------------+      | index-objects  |
 +-------------+           |                       |                |                   +----------------+
       |                   |                       |                |                        |
       |                   |                       |                |                        |
       +-------------------+-----------------------+----------------+------------------------+
       |
       |            +-------------------------------------+            +------------------------------+
       |            | secretary                           |            | librarian                    |
       |            |=====================================|            |==============================|
       |            | transaction-log-file-extension      | string     | snapshot-file-extension      | string
       |            | transaction-log-directory           | pathname   | snapshot-directory           | pathname
       |            | transaction-log-directory-backup    | pathname   | snapshot-directory-backup    | pathname
       |            | transaction-log                     | pathname   | snapshot                     | pathname
       |            | transaction-log-stream              | stream     | snapshot-serializer          |
       |            | transaction-hook                    | function   | snapshot-serialization-state |
       |            | transaction-log-serializer          |            | snapshot-deserializer        |
       |            | transaction-log-serialization-state |            +------------------------------+
       |            | transaction-log-deserializer        |                          |
       |            +-------------------------------------+                          |
       |                         |                                                   |
       |                         |                                                   |
       +-------------------------+---------------------------------------------------+
       |
       |
       V
+--------------------+          +-------------+
| pool               |          | transaction |
|====================|          |=============|
|--------------------|          | args        |
| snapshot           |          | function    |
| restore            |          +-------------+
| backup             |
| execut-transaction |
+--------------------+
       |
       |
       V
+--------------+
| guarded-pool |
|==============|
| guard        |
+--------------+
                </pre></p>
            </section-contents>
        </section-container>


        <section-contents title="CLASS図：古い">
            <p><pre style="font-size:12px; line-height:12px;">
  +-----------+     +------------------------+            +---------------------+
  | pool-core |     | pool-transaction       |            | pool-persistence    |
  |===========|     |========================|            |=====================|
  | memes     | ht  | transaction-hook       | function   | serializer          | function
  | indexes   | ht  | snapshot               | ht         | deserializer        | function
  | options   | ht  | transaction-log        | pathname   | serialization-state | ???
  +-----------+     | transaction-log-stream | stream     +---------------------+
       |            +------------------------+                        |
       |                         |                                    |
       |                         |                                    |
       +-------------------------+------------------------------------+
       |
       |
       V
+----------------+              +-------------+
| pool           |              | transaction |
|================|              |=============|
| directory      |              | args        |
| file-extension |              | function    |
+----------------+              +-------------+
       |
       |
       V
+--------------+
| guarded-pool |
|==============|
| guard        |
+--------------+
            </pre></p>
        </section-contents>
    </section-container>
</package_upanishad-pool_sec-root_describe>
