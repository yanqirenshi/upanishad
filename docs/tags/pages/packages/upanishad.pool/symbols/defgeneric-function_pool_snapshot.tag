<defgeneric-function_pool_snapshot class="symbol">
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <h1 class="title is-1"><b>SNAPSHOT</b></h1>
                <h2 class="subtitle">Genric Function</h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">Syntax</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">
                <symbol-syntax name="SNAPSHOT"
                               args="pool"
                               results="pool"></symbol-syntax>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">Method Signatures</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">Arguments and Values</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">Description</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">
                <p>スナップショットを作成する。</p>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">Examples</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">Affected By</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">Exceptional Situations</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">None.</div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">See Also</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">None.</div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-4">Notes</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">None.</div>
        </div>
    </section>
</defgeneric-function_pool_snapshot>
