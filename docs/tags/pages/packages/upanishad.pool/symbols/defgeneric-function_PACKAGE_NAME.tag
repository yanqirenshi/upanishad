<defgeneric-function_PACKAGE_NAME>
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">Genric Function <b>start</b></h1>
                <h2 class="subtitle"></h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Syntax</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Method Signatures</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Arguments and Values</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Description</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Examples</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Affected By</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Exceptional Situations</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">None.</div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">See Also</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">None.</div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Notes</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">None.</div>
        </div>
    </section>
</defgeneric-function_PACKAGE_NAME>
