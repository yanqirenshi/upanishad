<package_upanishad-pool_sec-root_operators>
    <section class="section">
        <div class="container">
            <h1 class="title">Operators</h1>
            <h2 class="subtitle"></h2>

            <section class="section">
                <div class="container">
                    <div class="contents">
                        <operators data={operators.pool}></operators>
                    </div>
                </div>
            </section>

            <section class="section">
                <div class="container">
                    <div class="contents">
                        <operators data={operators.meme}></operators>
                    </div>
                </div>
            </section>

            <section class="section">
                <div class="container">
                    <div class="contents">
                        <operators data={operators.index}></operators>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <script>
     this.operators = {
         pool: [
             {type: 'Function',         code: 'make-pool',       description: 'プールを作成します。'},
             {type: 'Generic Function', code: 'start',           description: 'トランザクションの記録を開始する。'},
             {type: 'Generic Function', code: 'stop',            description: 'トランザクションログの記録を停止します。'},
             {type: 'Generic Function', code: 'snapshot',        description: 'スナップショットを作成する。'},
             {type: 'Generic Function', code: 'restore',         description: 'スナップショットとトランザクションログから meme を戻す。'},
             {type: 'Generic Function', code: 'totally-destroy', description: 'いろいろ全部削除します。'},
         ],
         meme: [
             {type: 'Generic Function', code: 'tx-create-meme',  description: ''},
             {type: 'Generic Function', code: 'tx-update-meme',  description: ''},
             {type: 'Generic Function', code: 'tx-delete-meme',  description: ''},
             {type: 'Generic Function', code: 'find-meme',       description: ''},
             {type: 'Generic Function', code: 'get-meme',        description: ''},
         ],
         index: [
             {type: 'Generic Function', code: 'get-index',       description: 'upanishad.pool.core'},
             {type: 'Generic Function', code: 'tx-add-index',    description: 'upanishad.pool.core'},
             {type: 'Generic Function', code: 'tx-remove-index', description: 'upanishad.pool.core'},
         ]

     };

    </script>
</package_upanishad-pool_sec-root_operators>
