<package_upanishad-index_sec-root>
    <section-header title="Package: UPANISHAD.INDEX"></section-header>

    <section-container title="概要">
    </section-container>

    <section-container title="CLASSES">
        <section-contents title="CLASS図">
            <p>
                <pre style="font-size:12px; line-height:12px;">
+-------+
| index |
|=======|
+-------+
    |
    V
+--------------+
| slot-index   |
|==============|
| class-symbol |
| slot-symbol  |
| %id->value   |
+--------------+
    |
    +---------------------------------+
    |                                 |
    V                                 V
+-------------------+       +---------------------+
| slot-index-unique |       | slot-index-multiple |
|===================|       |=====================|
| value->object     |       | value->objects      |
+-------------------+       +---------------------+
                </pre>
            </p>
        </section-contents>
    </section-container>

    <section-container title="Operators">
        <section-contents title="一覧">
            <p>
                <pre style="font-size:12px; line-height:12px;">
| file                     | type             | symbol                       | args                                            | package         |
|--------------------------+------------------+------------------------------+-------------------------------------------------+-----------------|
| slot-index-multiple.lisp | function         | ensure-%id->object           | value->objects value                            | upanishad.index |
|                          | function         | slot-index-multiple-contexts | index meme                                      | upanishad.index |
|                          | function         | add-object-add-multi         | meme value %id %id->object %id->value           | upanishad.index |
|                          | generic function | add-object                   | index meme                                      | upanishad.index |
|                          | generic function | add-objects                  | index memes                                     | upanishad.index |
|                          | function         | add-object-remove-multi      | value %id %id->object %id->value value->objects | upanishad.index |
|                          | generic function | remove-object                | index meme                                      | upanishad.index |
|                          | generic function | change-object                | index meme                                      | upanishad.index |
|                          | generic function | get-at-value                 | index value                                     | upanishad.index |
| slot-index-unique.lisp   | generic function | add-object                   | index meme                                      | upanishad.index |
|                          | generic function | add-objects                  | index memes                                     | upanishad.index |
|                          | generic function | remove-object                | index meme                                      | upanishad.index |
|                          | function         | change-object-remove         | index value meme                                | upanishad.index |
|                          | function         | change-object-add            | index value meme                                | upanishad.index |
|                          | generic function | change-object                | index meme                                      | upanishad.index |
|                          | generic function | get-at-value                 | index value                                     | upanishad.index |
| slot-index.lisp          | generic function | get-index-key                | slot-index                                      | upanishad.index |
|                          | function         | make-slot-index              | class-symbol slot-symbol type &optional objects | upanishad.index |
| utility.lisp             | function         | get-slot-index-class         | type                                            | upanishad.index |
|                          | function         | assert-class                 | class meme                                      | upanishad.index |
                </pre>
            </p>
        </section-contents>
    </section-container>

</package_upanishad-index_sec-root>
