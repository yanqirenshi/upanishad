<package_upanishad-pool-core_sec-root_operators>
    <section class="section">
        <div class="container">
            <h1 class="title">Operators</h1>
            <h2 class="subtitle"></h2>

            <div class="contents">
                <operators data={operators}></operators>
            </div>
        </div>
    </section>

    <script>
     this.operators = [
         {type: 'Generic Function', code: 'get-index',       description: ''},
         {type: 'Generic Function', code: 'tx-add-index',    description: ''},
         {type: 'Generic Function', code: 'tx-remove-index', description: ''},
         {type: 'Generic Function', code: 'clear-indexes',   description: ''},
         {type: 'Generic Function', code: 'get-memes',       description: ''},
         {type: 'Generic Function', code: 'ensure-memes',    description: ''},
         {type: 'Generic Function', code: 'clear-memes',     description: ''},
     ];
    </script>
</package_upanishad-pool-core_sec-root_operators>
