<upanishad-pool-core>
    <script>
     this.mixin(MIXINS.page);

     this.on('mount', () => { this.draw(); });
     this.on('update', () => { this.draw(); });
    </script>
</upanishad-pool-core>
