<package_upanishad-meme_sec-root>
    <section-header title="Package: UPANISHAD.MEME"></section-header>

    <section-container title="概要">
    </section-container>

    <section-container title="CLASSES">
        <section-contents title="CLASS図">
            <p>
                <pre style="font-size:12px; line-height:12px;">
+---------+
| brahman |
|=========|
+---------+
    |
    V
+-------+
| atman |
|=======|
| %id   | integer
+-------+
    |
    V
+------+
| meme |
|======|
+------+
    |
    V
+-----------+
| blob      |
|===========|
| name      |
| size      |
| mime-type |
| keywords  |
+-----------+
                </pre>
            </p>
        </section-contents>
    </section-container>
</package_upanishad-meme_sec-root>
