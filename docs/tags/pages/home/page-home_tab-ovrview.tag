<page-home_tab-ovrview>

    <section class="section">
        <div class="container">
            <h1 class="title"></h1>
            <h2 class="subtitle"></h2>

            <div class="contents">

                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Package</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr each={obj in list()}>
                            <td>{obj.name}</td>
                            <td>{obj.package}</td>
                            <td>{obj.description}</td>
                        </tr>
                    </tbody>
                </table>

            </div>

        </div>
    </section>

    <script>
     this.list = () => {
         return [
             { name: '情報遺伝子',   package: 'meme',         description: '' },
             { name: 'プール',       package: 'pool',         description: '' },
             { name: 'インデックス', package: 'index',        description: '' },
             { name: '書記',         package: 'perpetuation', description: '' },
             { name: '図書館員',     package: 'librarian',    description: '' },
             { name: '永続',         package: 'persistence',  description: '' },
         ];
     };
    </script>

</page-home_tab-ovrview>
