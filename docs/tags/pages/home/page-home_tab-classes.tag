<page-home_tab-classes>

    <section class="section">
        <div class="container">
            <h1 class="title"></h1>
            <h2 class="subtitle"></h2>

            <div class="contents">

                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>File</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr each={obj in list()}>
                            <td>
                                <a href={idLink(obj.id)}>
                                    {obj.id}
                                </a>
                            </td>
                            <td>{obj.name}</td>
                            <td>{obj.description}</td>
                            <td>
                                <a href={sourceLink(obj.file)}>
                                    {obj.file}
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </section>

    <script>
     this.list = () => {
         return STORE.get('data.nodes.list').filter((d) => {
             return d._class == 'CL-CLASS';
         });
     }
     this.idLink = (v) => {
         return location.hash + '/classes/' + v;
     };
     this.sourceLink = (v) => {
         let head = 'https://gitlab.com/yanqirenshi/upanishad/blob/master/src/';

         return head + v;
     };
    </script>

</page-home_tab-classes>
