<page-home>

    <div class="page-root">
        <section-header title="Upanishad"></section-header>

        <div>
            <page-tabs core={page_tabs} callback={clickTab}></page-tabs>
        </div>

        <div class="tab-contents">
            <page-home_tab-ovrview   class="hide"></page-home_tab-ovrview>
            <page-home_tab-variables class="hide"></page-home_tab-variables>
            <page-home_tab-packages  class="hide"></page-home_tab-packages>
            <page-home_tab-classes   class="hide"></page-home_tab-classes>
            <page-home_tab-operators class="hide"></page-home_tab-operators>
        </div>
    </div>

    <script>
     this.page_tabs = new PageTabs([
         { code: 'ovrview',   label: 'Ovrview',   tag: 'page-home_tab-ovrview' },
         { code: 'packages',  label: 'Packages',  tag: 'page-home_tab-packages' },
         { code: 'variables', label: 'Variables', tag: 'page-home_tab-variables' },
         { code: 'classes',   label: 'Classes',   tag: 'page-home_tab-classes' },
         { code: 'operators', label: 'Operators', tag: 'page-home_tab-operators' },
     ]);

     this.on('mount', () => {

         let len = Object.keys(this.tags).length;
         if (len==0) // TODO: これはどんなとき？
             return;

         this.page_tabs.switchTab(this.tags)
         this.update();
     });

     this.clickTab = (e, action, data) => {
         if (this.page_tabs.switchTab(this.tags, data.code))
             this.update();
     };
    </script>

</page-home>
