<package_upanishad_sec-root_operators>
    <section class="section">
        <div class="container">
            <h1 class="title">Operators</h1>
            <h2 class="subtitle">
            </h2>

            <div class="contents">
                <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Symbol</th>
                            <th>Description</th>
                            <th>From Package</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr each={operators}>
                            <td>{type}</td>
                            <td><b>{symbol}</b></td>
                            <td>{description}</td>
                            <td>{from_package}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <script>
     this.operators = [
         {type:'Accessr',          symbol:'%id',            description:'', from_package: 'upanishad.meme'},
         {type:'Class',            symbol:'meme',           description:'', from_package: 'upanishad.meme'},
         {type:'Function',         symbol:'make-pool',      description:'Pool 作成', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'start',          description:'Pool トランザクションログ 記録開始 @未実装', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'stop',           description:'Pool トランザクションログ 記録停止 ', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'tx-create-meme', description:'meme 作成(C)', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'tx-delete-meme', description:'meme 削除(D)', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'find-meme',      description:'meme 検索(R)', from_package: 'upanishad.pool'},
         {type:'Generic function', symbol:'get-meme',       description:'meme 取得(R)', from_package: 'upanishad.pool'}
     ];
    </script>
</package_upanishad_sec-root_operators>
