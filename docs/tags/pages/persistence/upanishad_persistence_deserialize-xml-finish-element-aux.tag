<upanishad_persistence_deserialize-xml-finish-element-aux>
    <section-header title="Generic Function: DESERIALIZE-XML-NEW-ELEMENT"></section-header>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">Syntax:</h1>
            <div class="contents">
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">Method Signatures:</h1>
            <div class="contents">
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">Arguments and Values:</h1>
            <div class="contents">
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">Description:</h1>
            <div class="contents">
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">Examples:</h1>
            <div class="contents">
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">Affected By:</h1>
            <div class="contents">
                <p>None.</p>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">Exceptional Situations:</h1>
            <div class="contents">
                <p>None.</p>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">See Also:</h1>
            <div class="contents">
                <p>None.</p>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title is-3">Notes:</h1>
            <div class="contents">
                <p>None.</p>
            </div>
        </div>
    </section>
</upanishad_persistence_deserialize-xml-finish-element-aux>
