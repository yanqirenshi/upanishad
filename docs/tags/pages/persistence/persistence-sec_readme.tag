<persistence-sec_readme>
    <section class="section">
        <div class="container">
            <h1 class="title">Description</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">
                <p>meme のみ一意性が担保される。それ以外のオブジェクトは別物として扱われる。</p>
                <p>ハッシュテーブル、シーケンス、構造体もそう。</p>
                <p></p>
            </div>
        </div>
    </section>
</persistence-sec_readme>
