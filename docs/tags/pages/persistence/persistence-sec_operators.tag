<persistence-sec_operators>
    <section class="section">
        <div class="container">
            <div class="contents">
                <table class="table">
                    <thead>
                        <tr> <th>Type</th> <th>Symbol</th> <th>Description</th></tr>
                    </thead>
                    <tbody>
                        <tr each={operators}>
                            <td>{type}</td>
                            <td>
                                <a href="#persistence/{code}">{code}</a>
                            </td>
                            <td>{description}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <script>
     this.operators = [
         { code: 'serialize-xml-internal',             type: 'Generic Function', description: '' },
         { code: 'deserialize-xml-new-element',        type: 'Function',         description: '' },
         { code: 'deserialize-xml-new-element-aux',    type: 'Generic Function', description: '' },
         { code: 'deserialize-xml-finish-element',     type: 'Function',         description: '' },
         { code: 'deserialize-xml-finish-element-aux', type: 'Generic Function', description: '' },
         { code: 'reset',                              type: 'Generic Function', description: '' },
         { code: 'reset-known-slots',                  type: 'Generic Function', description: '' },
         { code: 'known-object-id',                    type: 'Generic Function', description: '' },
         { code: 'set-known-object',                   type: 'Generic Function', description: '' },
         { code: 'serializable-slots',                 type: 'Generic Function', description: '' },
         { code: 'get-serializable-slots',             type: 'Generic Function', description: '' },
         { code: 'get-serializable-slots',             type: 'Generic Function', description: '' },
         { code: 'sequence-type-and-length',           type: 'Function',         description: '' },
         { code: 'serialize-sexp-internal',            type: 'Generic Function', description: '' },
     ];
    </script>
</persistence-sec_operators>
