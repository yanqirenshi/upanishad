<persistence-sec_classes>
    <section class="section">
        <div class="container">
            <h1 class="title">Class: serialization-state</h1>
            <h2 class="subtitle"></h2>
            <div class="contents">
                <table class="table">
                    <thead>
                        <tr>
                            <th>name</th>
                            <th>accessor</th>
                            <th>reader</th>
                            <th>initform</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr each={class.slots}>
                            <td>{name}</td>
                            <td>{accessor}</td>
                            <td>{reader}</td>
                            <td>{initform}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <script>
     this.class = {
         slots: [
             { name: 'counter',     accessor: 'get-counter',                 initform: '0' },
             { name: 'hashtable',   accessor: null, reader: 'get-hashtable', initform: '(make-hash-table :test \'eq :size 1024 :rehash-size 2.0)' },
             { name: 'known-slots', accessor: null,                          initform: '(make-hash-table)' },
         ]
     }
    </script>
</persistence-sec_classes>
