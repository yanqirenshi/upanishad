<page-tabs2>
    <section class="section" style="padding-top: 0.5rem; padding-bottom: 0.5rem;">
        <div class="container">
            <div class="tabs">
                <ul>
                    <li each={opts.tabs}
                        class="{opts.active_tab==code ? 'is-active' : ''}">
                        <a code={code}
                           onclick={opts.clickTab}>{label}</a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
</page-tabs2>
