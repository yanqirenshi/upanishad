<persistence-sec_xml>
    <section class="section">
        <div class="container">
            <h1 class="title">Description</h1>
            <div class="contents">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Object</th>
                            <th>XML Tag</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr each={method_signatures}>
                            <td>{object}</td>
                            <td>{tag}</td>
                            <td>{description}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Serialize</h1>
            <div class="contents">
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Description</h1>
            <div class="contents">
            </div>
        </div>
    </section>

    <script>
     this.method_signatures = [
         { object: 'null',             tag: '<NULL/>',                                   description: '' },
         { object: 't',                tag: '<SYMBOL></SYMBOL>',                         description: '' },
         { object: 'symbol',           tag: '<TRUE/>',                                   description: '' },
         { object: 'string',           tag: '<STRING></STRING>',                         description: '' },
         { object: 'character',        tag: '<CHARACTER></CHARACTER>',                   description: '' },
         { object: 'complex',          tag: '<COMPLEX></COMPLEX>',                       description: '' },
         { object: 'float',            tag: '<FLOAT></FLOAT>',                           description: '' },
         { object: 'integer',          tag: '<INT></INT>',                               description: '' },
         { object: 'ratio',            tag: '<RATIO></RATIO>',                           description: '' },
         { object: 'hash-table',       tag: '<HASH-TABLE TEST="" SIZE=""></HASH-TABLE>', description: 'IDは削除しました。' },
         { object: 'sequence',         tag: '<SEQUENCE CLASS="" SIZE=""></SEQUENCE>',    description: 'IDは削除しました。' },
         { object: 'standard-object',  tag: '<OBJECT CLASS=""></OBJECT>',                description: 'IDは削除しました。' },
         { object: 'structure-object', tag: '<STRUCT CLASS=""></STRUCT>',                description: 'IDは削除しました。' },
         { object: 'slot-index',       tag: '<INDEX CLASS=""></INDEX>',                  description: '' },
         { object: 'memes',            tag: '<MEMES CLASS=""></MEMES>',                  description: '' },
         { object: 'meme',             tag: '<MEME ID="" CLASS=""></MEME>',              description: '' },
     ]
    </script>
</persistence-sec_xml>
