<upanishad_persistence_serialize-xml-internal>
    <section-header title="Generic Function: SERIALIZE-SEXP-INTERNAL"></section-header>

    <section class="section">
        <div class="container">
            <h1 class="title">Syntax:</h1>
            <div class="contents">
                <p>serialize-xml-internal object stream serialization-state ⇒ ???</p>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Method Signatures:</h1>
            <div class="contents">
                <table class="table">
                    <thead>
                        <tr>
                            <th>object</th>
                            <th>stream</th>
                            <th>serializatio_state</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr each={method_signatures}>
                            <td>{object}</td>
                            <td>{stream}</td>
                            <td>{serializatio_state}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Arguments and Values:</h1>
            <h2 class="subtitle"></h2>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Description:</h1>
            <div class="contents">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Object</th>
                            <th>XML Tag</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr each={method_signatures}>
                            <td>{object}</td>
                            <td>{tag}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Examples:</h1>
            <div class="contents"><p>None.</p></div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Affected By:</h1>
            <div class="contents"><p>None.</p></div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">Exceptional Situations:</h1>
            <div class="contents"><p>None.</p></div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <h1 class="title">See Also:</h1>
            <div class="contents"><p>None.</p></div>
        </div>
    </section>

    <script>
     this.method_signatures = [
         { object: 'null',             stream: null, serializatio_state: null, tag: '<NULL/>' },
         { object: 't',                stream: null, serializatio_state: null, tag: '<SYMBOL></SYMBOL>' },
         { object: 'symbol',           stream: null, serializatio_state: null, tag: '<TRUE/>' },
         { object: 'string',           stream: null, serializatio_state: null, tag: '<STRING></STRING>' },
         { object: 'character',        stream: null, serializatio_state: null, tag: '<CHARACTER></CHARACTER>' },
         { object: 'complex',          stream: null, serializatio_state: null, tag: '<COMPLEX></COMPLEX>' },
         { object: 'float',            stream: null, serializatio_state: null, tag: '<FLOAT></FLOAT>' },
         { object: 'integer',          stream: null, serializatio_state: null, tag: '<INT></INT>' },
         { object: 'ratio',            stream: null, serializatio_state: null, tag: '<RATIO></RATIO>' },
         { object: 'hash-table',       stream: null, serializatio_state: null, tag: '<HASH-TABLE ID="" TEST="" SIZE=""></HASH-TABLE>' },
         { object: 'sequence',         stream: null, serializatio_state: null, tag: '<SEQUENCE ID="" CLASS="" SIZE=""></SEQUENCE>' },
         { object: 'standard-object',  stream: null, serializatio_state: null, tag: '<OBJECT ID="" CLASS=""></OBJECT>' },
         { object: 'structure-object', stream: null, serializatio_state: null, tag: '<STRUCT ID="" CLASS=""></STRUCT>' },
         { object: 'slot-index',       stream: null, serializatio_state: null, tag: '<INDEX CLASS=""></INDEX>' },
         { object: 'memes',            stream: null, serializatio_state: null, tag: '<MEMES CLASS=""></MEMES>' },
         { object: 'meme',             stream: null, serializatio_state: null, tag: '<MEME ID="" CLASS=""></MEME>' },
     ]
    </script>
</upanishad_persistence_serialize-xml-internal>
