<persistence-sec_root>
    <section-header title="永続化"></section-header>

    <page-tabs2 tabs={tabs} active_tab={active_tab} click-tab={clickTab}></page-tabs2>

    <div>
        <persistence-sec_readme class="hide"></persistence-sec_readme>
        <persistence-sec_classes class="hide"></persistence-sec_classes>
        <persistence-sec_operators class="hide"></persistence-sec_operators>
        <persistence-sec_xml class="hide"></persistence-sec_xml>
    </div>

    <script>
     this.tabs = [
         { code: 'readme',    label: 'Readme',    tag: 'persistence-sec_readme' },
         { code: 'classes',   label: 'Classes',   tag: 'persistence-sec_classes' },
         { code: 'operators', label: 'Operators', tag: 'persistence-sec_operators' },
         { code: 'xml',       label: 'Xml', tag: 'persistence-sec_xml' },
     ];
     this.active_tab = null;
     this.clickTab = (e) => {
         let code = e.target.getAttribute('code');

         this.switchTab(code);

         this.update();
     };
     this.switchTab = (code) => {
         if (this.active_tab==code)
             return;

         let active = null;
         for (var i in this.tabs) {
             let tab = this.tabs[i];
             let tag = this.tags[tab.tag];

             tag.root.classList.add('hide');

             if (tab.code==code)
                 active = tag;
         }

         if (active)
             active.root.classList.remove('hide');

         this.active_tab = code;
     };
     this.on('mount', () => {
         this.switchTab(this.tabs[0].code);
     });
    </script>
</persistence-sec_root>
