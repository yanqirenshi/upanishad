<usage-sec_root_pool-basic class="usage-section-root">
    <section class="section">
        <div class="container">
            <h1 class="title is-4">POOL</h1>
            <h2 class="subtitle"></h2>
        </div>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">作成</h1>
                <h2 class="subtitle"></h2>

                <div class="content">
                    <p><pre>
(in-package :upanishad)
(make-pool #P"~/tmp/")
                    </pre></p>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">トランザクションログの収集開始</h1>
                <h2 class="subtitle"></h2>
                <article class="content">
                    <p>特定のオペレーションはなく make-pool したタイミングからトランザクションログの記録が開始します。</p>
                    <p>しかし、stop オペレータがあるので start があっても良さそう。</p>
                </article>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">トランザクションログの収集停止</h1>
                <h2 class="subtitle"></h2>
                <article class="content">
                    <p>この操作でトランザクションログの記録が停止されます</p>
                    <p><pre>(stop (upanishad:make-pool #P"~/tmp/"))</pre></p>
                </article>
            </div>
        </section>
    </section>
</usage-sec_root_pool-basic>
