<usage-sec_root_meme-basic class="usage-section-root">
    <section class="section">
        <div class="container">
            <h1 class="title is-4">meme の追加/変更/削除</h1>
            <h2 class="subtitle">
            </h2>
        </div>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">Meme の追加</h1>
                <h2 class="subtitle">
                </h2>
                <div class="content">
                    <p><pre>
(defclass user (meme)
  ((code :accessor code :initarg :code :initform nil)
   (name :accessor name :initarg :name :initform "")))
                    </pre></p>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">Meme の取得</h1>
                <h2 class="subtitle">TX-CREATE-MEME を利用します。</h2>
                <div class="content">
                    <p><pre>
(tx-create-meme *pool* 'user
                '((code . :user01)
                  (name . "test-user-01")))
(tx-create-meme *pool* 'user
                '((code . :user02)
                  (name . "test-user-01")))
                    </pre></p>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">Meme の検索</h1>
                <h2 class="subtitle">
                </h2>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">Meme の削除</h1>
                <h2 class="subtitle">GET-MEME を利用します。</h2>
                <div class="content">
                    <p><pre>
(get-meme *pool* 'user :%id 1)
(get-meme *pool* 'user :%id 2)
                    </pre></p>
                </div>
            </div>
        </section>
    </section>
</usage-sec_root_meme-basic>
