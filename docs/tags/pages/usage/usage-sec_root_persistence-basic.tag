<usage-sec_root_persistence-basic class="usage-section-root">
    <section class="section">
        <div class="container">
            <h1 class="title is-4">永続化</h1>
            <h2 class="subtitle">
            </h2>
        </div>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">スナップショット</h1>
                <h2 class="subtitle">
                </h2>
                <div class="content">
                    <p><pre>(snapshot *pool*)</pre></p>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <h1 class="title is-5">レストア</h1>
                <h2 class="subtitle">
                </h2>
                <div class="content">
                    <p><pre>(restore *pool*)</pre></p>
                </div>
            </div>
        </section>

    </section>
</usage-sec_root_persistence-basic>
