<app-global-menu-item>

    <div class="menu-item-root {isActive()}">

        <div class="menu-item-text"
             code={opts.source.code}
             onclick={clickItem}>
            {opts.source.menu_label}
        </div>

    </div>

    <script>
     this.clickItem = (e) => {
         let code = e.target.getAttribute('code');

         location.hash = '#' + code;
     };
     this.isActive = (obj) => {
         return this.opts.active_page == opts.source.code ? 'active' : '';
     };
    </script>

    <style>
     app-global-menu-item .menu-item-root {
         display: block;
         word-break: keep-all;
         margin-bottom: 3px;

         padding-left: 11px;
         padding-right: 11px;
     }

     app-global-menu-item .menu-item-text {
         padding: 6px 8px;
         border-radius: 3px;

         display: flex;
         justify-content: center;
         align-items: center;

         font-weight: bold;
         color: #BFA742;
         background: #830411;
     }

     app-global-menu-item .menu-item-text:hover {
         color: #F7DE59;
     }
     app-global-menu-item .menu-item-root.active .menu-item-text {
         color: #830411;
         background: #F7DE59;
     }
    </style>

</app-global-menu-item>
