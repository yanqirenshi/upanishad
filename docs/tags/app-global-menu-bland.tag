<app-global-menu-bland>

    <div>
        <div>
            UP
        </div>
    </div>

    <style>
     app-global-menu-bland > div {
         display: flex;
         justify-content: center;
         align-items: center;
         margin-top: 22px;
         margin-bottom: 22px;
     }

     app-global-menu-bland > div > div{
         display: flex;
         justify-content: center;
         align-items: center;

         width:33px;
         height:33px;

         border-radius: 3px;

         font-weight: bold;
         background: #BFA742;
         color: #830411;
     }
    </style>

</app-global-menu-bland>
