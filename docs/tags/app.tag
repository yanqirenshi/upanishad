<app>

    <div class="flex-root">

        <div class="flex-left">
            <!-- <menu-bar brand={{label:'UP'}} site={site()} moves={[]}></menu-bar> -->
            <app-global-menu></app-global-menu>
        </div>

        <div class="flex-right">
            <app-page-area></app-page-area>
        </div>

    </div>

    <script>
     this.site = () => {
         return STORE.state().get('site');
     };
     this.updateMenuBar = () => {
         if (this.tags['menu-bar'])
             this.tags['menu-bar'].update();
     }
     STORE.subscribe((action)=>{
         if (action.type=='MOVE-PAGE') {
             this.updateMenuBar();

             this.tags['app-global-menu'].update();
             this.tags['app-page-area'].update({ opts: { route: action.route }});
         }
     })

     this.on('mount', () => {
         let route = location.hash.substring(1).split('/');

         ACTIONS.movePage({ route: route });
     });

     window.addEventListener('resize', (event) => {
         this.update();
     });

     if (location.hash=='')
         location.hash='#home'
    </script>

    <style>
     app > .flex-root {
         display: flex;
         width: 100vw;
         height: 100vh;
     }
     app > .flex-root > .flex-left {
         /* width: 55px; */
     }
     app > .flex-root > .flex-right {
         flex-grow: 1;
     }
    </style>

</app>
