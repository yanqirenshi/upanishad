<symbol-syntax>
    <div class="syntax">
        <b style="color: #888888;">{opts.name}</b>

        <i>{opts.args}</i>

        <span class={diplay('keys')}
              style="color: #888888;">&key</span>

        <i class={diplay('keys')}>{opts.keys}</i>

        <span class={diplay('optional')}
              style="color: #888888;">&optional</span>

        <i class={diplay('optional')}>{opts.optional}</i>

        <span style="color: #888888;">&rArr;</span>

        <i>{opts.results}</i>
    </div>

    <style>
     symbol-syntax .hiden { display: none; }
     symbol-syntax .syntax > * { margin-right: 8px; }
    </style>

    <script>
     this.diplay = (code) => {
         return opts[code] ? '' : 'hiden';
     }
    </script>
</symbol-syntax>
