<operators>
    <table class="table is-bordered is-striped is-narrow is-hoverable">
        <thead>
            <tr>
                <th>Type</th>
                <th>Name</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <tr each={opts.data}>
                <td>{type}</td>
                <td><a href="#upanishad-pool/{code}">{code.toUpperCase()}</a></td>
                <td>{description}</td>
            </tr>
        </tbody>
    </table>
</operators>
