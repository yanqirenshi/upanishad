(defpackage :upanishad-test.utility
  (:use #:cl
        #:prove
        #:upanishad)
  (:export #:with-pool
           #:test-meme
           #:*test-pool-directory*
           #:name)
  (:export #:is-class
           #:is-%id
           #:is-%ids))
(in-package :upanishad-test.utility)


;;;;;
;;;;; directory
;;;;;
(defparameter *test-pool-directory*
  (merge-pathnames "t/data/" (asdf:system-source-directory :upanishad-test)))


;;;;;
;;;;; with-pool
;;;;;
(defmacro with-pool ((pool directory) &body body)
  `(let ((,pool nil))
     (unwind-protect
          (progn
            (setf ,pool (make-pool ,directory))
            (totally-destroy ,pool)
            (start ,pool)
            ,@body)
       (when ,pool
         (stop ,pool)))))


;;;;;
;;;;; classe
;;;;;
(defclass test-meme (upanishad:meme)
  ((name :accessor name
         :initarg :name
         :initform "")))


;;;;;
;;;;; matcher
;;;;;
(defun is-class (got expect)
  (is (class-name (class-of got))
      expect))

(defun is-%id (got expect)
  (is (%id got)
      (%id expect)))

(defun is-%ids (gots expects)
  (labels ((%ids (memes)
             (sort (mapcar #'%id memes) #'<)))
    (is (%ids gots)
        (%ids expects))))
