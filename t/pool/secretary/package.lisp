(defpackage :upanishad-test.pool.secretary
  (:nicknames :up-test.pool.secretary)
  (:use #:cl
        #:prove
        #:upanishad-test.utility
        #:upanishad.pool.secretary))
(in-package :upanishad-test.pool.secretary)

(defparameter *secretary-dir* "~/prj/fg/upanishad/t/data/pool/secretary")

(defclass test-secretary (secretary)
  ((name :accessor name :initarg :name :initform nil)))

(defun make-secretary ()
  (make-instance 'test-secretary
                 :transaction-log-directory *secretary-dir*
                 :transaction-log-directory-backup *secretary-dir*))

(defmacro with-secretary ((secretary) &body body)
  `(let ((,secretary nil))
     (unwind-protect
          (progn
            (setf ,secretary (make-secretary))
            (init-secretary ,secretary)
            ,@body))))
