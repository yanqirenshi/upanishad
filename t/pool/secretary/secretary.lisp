(in-package :upanishad-test.pool.secretary)

(subtest "secretary"
  (let ((secretary (make-instance 'test-secretary)))
    (is-class secretary 'test-secretary)))

(subtest "init-secretary"
  (let ((secretary (make-instance 'test-secretary
                                  :transaction-log-directory *secretary-dir*
                                  :transaction-log-directory-backup *secretary-dir*)))
    (is-class (init-secretary secretary)
              'test-secretary)))

(subtest "open-transaction-log-stream"
  (with-secretary (secretary)
    (is-class (open-transaction-log-stream secretary)
              'test-secretary)))

(subtest "close-transaction-log-stream"
  (with-secretary (secretary)
    (open-transaction-log-stream secretary)
    (is-class (close-transaction-log-stream secretary)
              'test-secretary)))
