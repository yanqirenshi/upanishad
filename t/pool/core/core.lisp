(in-package :upanishad-test.pool.core)

;;;;;
;;;;; with-pool-core
;;;;;
(defmacro with-pool-core ((pool) &body body)
  `(let ((,pool nil))
     (unwind-protect
          (progn
            (setf ,pool (make-instance 'pool-core))
            ,@body))))

;;;;;
;;;;; with-pool
;;;;;
(subtest "POOL-CORE"
  (let ((pool (make-instance 'pool-core)))
    (subtest "MEMES"
      (let ((memes (memes pool)))
        (ok memes)
        (is (type-of memes) 'HASH-TABLE)))
    (subtest "INDEXES"
      (let ((indexs (indexes pool)))
        (ok indexs)
        (is (type-of indexs) 'HASH-TABLE)))))

(defclass test-class-1 ()
  ((name :accessor name :initarg :name :initform nil)))

(defclass test-class-2 ()
  ((name :accessor name :initarg :name :initform nil)))

;;;;;
;;;;; INDEXES
;;;;;
(subtest "TX-ADD-INDEX"
  (subtest "pool-core list"
    (with-pool-core (pool)
      (let ((index (tx-add-index pool '(:class test-class-1 :slot name :type :unique))))
        (is (class-name (class-of index))
            'upanishad.index:slot-index-unique)))))

(subtest "GET-INDEX"
  (subtest "pool-core list"
    (with-pool-core (pool)
      (let ((index (tx-add-index pool '(:class test-class-1 :slot name :type :unique))))
        (is (get-index pool 'test-class-1 'name)
            index)))))

(subtest "TX-REMOVE-INDEX"
  (with-pool-core (pool)
    (let ((index (tx-add-index pool '(:class test-class-1 :slot name :type :unique))))
        (is (class-name (class-of index))
            'upanishad.index:slot-index-unique)
        (is (get-index pool 'test-class-1 'name)
            index)
      (tx-remove-index pool index)
      (is (get-index pool 'test-class-1 'name)
          nil))))

(subtest "ENSURE-INDEX"
  (skip 1 "未実装"))

(subtest "CLEAR-INDEXES"
  (with-pool-core (pool)
    (ok (tx-add-index pool '(:class test-class-1 :slot name :type :unique)))
    (ok (tx-add-index pool '(:class test-class-2 :slot name :type :unique)))
    (clear-indexes pool)
    (is (get-index pool 'test-class-1 'name) nil)
    (is (get-index pool 'test-class-2 'name) nil)))

;;;;;
;;;;; MEMES
;;;;;
(subtest "GET-MEMES"
  (with-pool-core (pool)
    (let ((memes (get-memes pool :class 'test-class-1)))
      (print memes))))

(subtest "TX-ADD-INDEX"
  (skip 1 "未実装"))

(subtest "TX-REMOVE-INDEX"
  (skip 1 "未実装"))

(subtest "ENSURE-MEMES"
  (with-pool-core (pool)
    (print (ensure-memes pool :class 'test-class-1))))

(subtest "CLEAR-MEMES")
