(in-package :upanishad-test.pool.librarian)

(defclass test-librarian (librarian)
  ((objects
    :accessor objects
    :initarg :objects
    :initform (make-hash-table)
    :type 'hash-table)))

(defparameter *snapshot-types* '((:objects . objects)))
(defparameter *snapshot-dir* #P"~/prj/fg/upanishad/t/data/pool/librarian/")

(defun make-librarian ()
  (make-instance 'test-librarian
                 :snapshot-directory *snapshot-dir*
                 :snapshot-directory-backup *snapshot-dir*))

;;;;;
;;;;; with-pool-core
;;;;;
(defmacro with-librarian ((librarian) &body body)
  `(let ((,librarian nil))
     (unwind-protect
          (progn
            (setf ,librarian (make-librarian))
            (init-librarian ,librarian *snapshot-types*)
            ,@body))))

(subtest "librarian"
  (let ((librarian (make-librarian)))
    (is (class-name (class-of librarian))
        'test-librarian)))

(subtest "init-librarian"
  (let ((librarian (make-librarian)))
    (init-librarian librarian *snapshot-types*)))

(subtest "snapshot-objects"
  (with-librarian (librarian)
    (snapshot-objects librarian *snapshot-types*)))

(subtest "restore-snapshots"
  (with-librarian (librarian)
    (is-class (restore-snapshots librarian *snapshot-types*)
              'test-librarian)))

(subtest "backup-snapshots"
  (with-librarian (librarian)
    (snapshot-objects librarian *snapshot-types*)
    (is-class (backup-snapshots librarian *snapshot-types*)
              'test-librarian)))

(subtest "clear-snapshots"
  (with-librarian (librarian)
    (snapshot-objects librarian *snapshot-types*)
    (is-class (clear-snapshots librarian)
              'test-librarian)))

(subtest "clear-snapshot-backups"
  (with-librarian (librarian)
    (snapshot-objects librarian *snapshot-types*)
    (backup-snapshots librarian *snapshot-types*)
    (is-class (clear-snapshot-backups librarian)
              'test-librarian)))
