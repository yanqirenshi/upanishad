(in-package :upanishad-test)

(plan nil)

(diag "slot-index-unique.lisp")

(subtest "slot-index-multiple"
  (with-pool (pool *test-pool-directory*)
    (subtest "ensure-%id->object"
      (skip 1 "wait ...."))

    (subtest "slot-index-multiple-contexts"
      (skip 1 "wait ...."))

    (subtest "add-object-add-multi"
      (skip 1 "wait ...."))

    (subtest "add-object"
      (skip 1 "wait ...."))

    (subtest "add-objects"
      (skip 1 "wait ...."))

    (subtest "add-object-remove-multi"
      (skip 1 "wait ...."))

    (subtest "remove-object"
      (skip 1 "wait ...."))

    (subtest "change-object"
      (skip 1 "wait ...."))

    (subtest "get-at-value"
      (skip 1 "wait ...."))))

(finalize)
