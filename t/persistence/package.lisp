(defpackage :upanishad-test.persistence
  (:nicknames :up-test.persistence)
  (:use #:cl
        #:prove
        #:upanishad.persistence
        #:upanishad-test.utility))
(in-package :upanishad-test.persistence)
