(defpackage :upanishad-test.memes
  (:nicknames :up-test.memes)
  (:use #:cl
        #:prove
        #:upanishad-test.utility
        #:upanishad.memes))
(in-package :upanishad-test.memes)

(defclass test-meme (upanishad.meme:meme)
  ((name :accessor name :initarg :name :initform nil)))

(subtest "memes"
  (let ((memes (make-instance 'memes)))
    (print memes)))

(subtest "meme-class"
  (let ((memes (make-instance 'memes)))
    (print (meme-class memes))))

(subtest "meme-list"
  (let ((memes (make-instance 'memes)))
    (print (meme-list memes))))

(subtest "%id-index"
  (let ((memes (make-instance 'memes)))
    (print (%id-index memes))))

(subtest "make-memes"
  (let ((memes (make-memes 'test-meme)))
    (print memes)))

(subtest "get-meme"
  (let ((memes (make-instance 'memes)))
    (print (get-meme memes :%id 1))))

(subtest "find-meme"
  (let ((memes (make-memes 'test-meme)))
    (print (find-meme memes :slot 'name :value "zzz"))))

(subtest "add-meme"
  (let ((memes (make-memes 'test-meme)))
    (print (add-meme memes (make-instance 'test-meme :%id 1)))))

(subtest "remove-meme"
  (let ((memes (make-memes 'test-meme))
        (meme (make-instance 'test-meme :%id 1)))
    (add-meme memes meme)
    (print (remove-meme memes meme))))
