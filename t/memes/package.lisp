(defpackage :upanishad-test.memes
  (:nicknames :up-test.memes)
  (:use #:cl
        #:prove
        #:upanishad-test.utility))
(in-package :upanishad-test.memes)
