(defpackage :upanishad-test
  (:nicknames :up-test)
  (:use #:cl
        #:prove
        #:upanishad
        #:upanishad-test.utility))
(in-package :upanishad-test)

(setf prove:*default-reporter* :list)
