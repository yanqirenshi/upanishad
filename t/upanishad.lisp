(in-package :upanishad-test)

(plan 3)

(subtest "POOL"
  (subtest "MAKE-POOL"
    (let ((pool (make-pool *test-pool-directory*)))
      (is (not (null pool)) t)
      (is (class-name (class-of pool)) 'up.pool:pool)
      (is (upanishad.pool.secretary::transaction-log-stream pool)  nil)))

  (subtest "START & STOP"
    (let ((pool (make-pool *test-pool-directory*)))
      (start pool)
      (is (not (upanishad.pool.secretary::transaction-log-stream pool))  nil)
      (stop pool)
      (is (upanishad.pool.secretary::transaction-log-stream pool)  nil)))

  (subtest "SNAPSHOT"
    (skip 1 "wait..."))

  (subtest "RESTORE"
    (skip 1 "wait..."))

  (subtest "TOTALLY-DESTROY"
    (skip 1 "wait...")))

(subtest "MEME"
  (subtest "TX-CREATE-MEME"
    (with-pool (pool *test-pool-directory*)
      (let ((meme (tx-create-meme pool 'test-meme)))
        (ok meme "tx-create-meme"))))

  (subtest "TX-UPDATE-MEME"
    (with-pool (pool *test-pool-directory*)
      (let ((meme (tx-create-meme pool 'test-meme '((name . "name-1")))))
        (tx-update-meme pool meme '((name . "name-x")))
        (is (name meme) "name-x"))))

  (subtest "TX-DELETE-MEME"
    (with-pool (pool *test-pool-directory*)
      (let ((meme (tx-create-meme pool 'test-meme '((name . "name-1")))))
        (tx-delete-meme pool 'test-meme (up:%id meme))
        (is (get-meme pool 'test-meme :%id (up:%id meme))
            nil))))

  (subtest "GET-MEME"
    (with-pool (pool *test-pool-directory*)
      (let ((meme1 (tx-create-meme pool 'test-meme '((name . "name-1"))))
            (meme2 (tx-create-meme pool 'test-meme '((name . "name-2"))))
            (meme3 (tx-create-meme pool 'test-meme '((name . "name-3")))))
        (declare (ignore meme1 meme3))
        (subtest "by %id"
          (is (get-meme pool 'test-meme :%id (up:%id meme2))
              meme2)))))

  (subtest "FIND-MEME"
    (with-pool (pool *test-pool-directory*)
      (let ((meme1 (tx-create-meme pool 'test-meme '((name . "name-1"))))
            (meme2 (tx-create-meme pool 'test-meme '((name . "name-2"))))
            (meme3 (tx-create-meme pool 'test-meme '((name . "name-3")))))
        (declare (ignore meme1 meme3))
        (is-%ids (find-meme pool 'test-meme :slot 'name :value "name-2")
                 (list meme2))))))

(subtest "INDEX"
  (subtest "TX-ADD-INDEX"
    (with-pool (pool *test-pool-directory*)
      (let ((meme1 (tx-create-meme pool 'test-meme '((name . "name-1"))))
            (meme2 (tx-create-meme pool 'test-meme '((name . "name-2"))))
            (meme3 (tx-create-meme pool 'test-meme '((name . "name-3")))))
        (declare (ignore meme1 meme2 meme3))
        (ok (tx-add-index pool '(:class test-meme :slot name :type :unique))))))

  (subtest "GET-INDEX"
    (with-pool (pool *test-pool-directory*)
      (let ((meme1 (tx-create-meme pool 'test-meme '((name . "name-1"))))
            (meme2 (tx-create-meme pool 'test-meme '((name . "name-2"))))
            (meme3 (tx-create-meme pool 'test-meme '((name . "name-3")))))
        (declare (ignore meme1 meme2 meme3))
        (tx-add-index pool '(:class test-meme :slot name :type :unique))
        (ok (get-index pool 'test-meme 'name)))))

  (subtest "TX-REMOVE-INDEX"
    (with-pool (pool *test-pool-directory*)
      (let ((meme1 (tx-create-meme pool 'test-meme '((name . "name-1"))))
            (meme2 (tx-create-meme pool 'test-meme '((name . "name-2"))))
            (meme3 (tx-create-meme pool 'test-meme '((name . "name-3")))))
        (declare (ignore meme1 meme2 meme3))
        (tx-add-index pool '(:class test-meme :slot name :type :unique))
        (ok (get-index pool 'test-meme 'name))
        (tx-remove-index pool (get-index pool 'test-meme 'name))
        (ok (not (get-index pool 'test-meme 'name)))))))

(finalize)
